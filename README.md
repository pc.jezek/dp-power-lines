# PowerLine3D
This project was developed as a diploma thesis on the Faculty of Information Technology, Czech Technical University in Prague.
Full text of the thesis is available at https://dspace.cvut.cz/handle/10467/99538.


This repository contains an implementation of a fully automatic pipeline 
for 3D power line reconstruction from UAV images. It is integrated with
the OpenDroneMap open-source photogrammetry software, which provides
the 3D reconstruction of the terrain. Combining the outputs enables vegetation 
encroachment detection in power line corridors, which can be visualized in CloudCompare.


Watch my PyData talk for a quick overview:
<div align="center">
  <a href="https://www.youtube.com/watch?v=zKjYUnT7DRk">
    <img src="examples/pydata-thumbnail.jpg" alt="Radek Ježek - Using drone imagery to detect vegetation around power lines [PyData Prague 2022-06-27]" height=300px>
  </a>
</div>

# Installation Guide
The application is written in the Python programming language, and 
was tested in the official `Ubuntu 20.04.3 LTS` docker image with
Python version `3.9.5` and `CUDA 11.5`.

## Prerequisities
A GPU with CUDA driver is required, the application was tested with a Tesla V100 32GB and CUDA 11.5.

Several system-wide dependencies are required:
- [*Python*](https://www.python.org/) >= 3.8, < 3.10,
- [*Pip*](https://pip.pypa.io/en/stable/), version corresponding to Python version,
- [*Python-venv*](https://docs.python.org/3/library/venv.html), for creating virtual environments,
- [*Opencv-python*](https://docs.opencv.org/), for OpenCV libraries,
- [*Git*](https://git-scm.com/), for cloning the repository,
- [*Git-LFS*](https://git-lfs.github.com/), for downloading large files from git,
- [*Poetry*](https://python-poetry.org/docs/), modern Python dependency and virtual environment manager,
- [*Poe the Poet*](https://github.com/nat-n/poethepoet), for running tasks within a Poetry virtual environment,
- [*curl*](https://curl.se/), for downloading Poetry installation script.

Optionally, for downloading and unzipping datasets or neural net training data:
- [zip](http://infozip.sourceforge.net/Zip.html), for unzipping datasets,
- [wget](https://www.gnu.org/software/wget/), for downloading datasets.

Please follow the official installation instructions for each of these packages.
On Ubuntu `20.04.3 LTS`, the packages can be installed using the following commands: 

```
# Install GIT, GIT-LFS, Python, curl
sudo apt update
sudo apt-get install \
    git \
    git-lfs \
    python3.9 \
    python3-pip \
    python3.9-venv \
    python3-opencv \
    curl

# Install Poetry
curl -sSL https://install.python-poetry.org | python3.9 -

# Add poetry to PATH (assuming bash shell)
echo "$PATH" | grep -q '\.local/bin' || \
(echo 'export PATH=~/.local/bin:$PATH' >> ~/.bashrc)
source ~/.bashrc

# Install Poe the Poet
pip3 install poethepoet
```
Optionally, for downloading datasets:
```
sudo apt-get install \
    wget \
    zip
```

Next, we require a running OpenDroneMap [NodeODM](https://github.com/OpenDroneMap/NodeODM) docker container:
First, [install docker using the official guide](https://docs.docker.com/engine/install/ubuntu/)
Then, download and run official OpenDroneMap  container NodeODM:
```
docker run -p 3000:3000 opendronemap/nodeodm
```
It can also be running remotely, in the cloud, or on a remote Linux cluster. NodeODM will require at least 128GB of RAM.

For visualization, install the [CloudCompare](http://www.cloudcompare.org/) point cloud editor using
the official installation instructions.

## Installation
Clone repository:
```
git clone https://gitlab.com/pc.jezek/dp-power-lines.git
cd dp-power-lines/
```
Install dependencies through Poetry and Poe (assuming neural network segmentation with CUDA GPU):
```
# Install regular dependencies from PyPI 
poetry install

# Download git-lfs files, PyTorch and MMCV for neural network segmentation 
# Assuming CUDA version 11.1+
# (for other versions, please follow the official Pytorch and MMCV documentation)
poe install

# Alternatively, if no supported GPU is available, or filter-based segmentation is sufficient,
# the following will omit neural network segmentation dependencies
poe install-filter-only
```

# User Guide
The application consists of three main parts:
- Photogrammetric pipeline, for which the [OpenDroneMap](https://opendronemap.org/) software is used
- Power line 3D reconstruction pipeline
- Visualization using [CloudCompare](http://www.cloudcompare.org/)

The architecture of the application with the input/output dependencies is visualized below:

![Vegetation Management Pipeline Architecture](examples/architecture.jpg)

Photogrammetric pipeline output (OpenDroneMap), 
and power line reconstruction pipeline output:

![Terrain Point Cloud](examples/terrain.jpg)
![Power Line Point Cloud](examples/power-lines.jpg)

## Prerequisities
First, check that all the required packages are installed, and the OpenDroneMap NodeODM 
server is running (usually at http://localhost:3000). Make sure, that the NodeODM server
host, and port are configured properly in **config/power_lines_cfg.yaml**, and that
NodeODM has enough RAM available (We recommend 200GB and more for trouble-free reconstruction). 
You may also adjust other settings in **config/power_lines_cfg.yaml** for different results,
for example switch to filter-based segmentation instead of neural network. The options
are explained in detail right in this file.

## Running All Steps
You may choose to run all steps at once for a fully automatic reconstruction.
Place your UAV images in the **data/images** folder (TIF format recommended).
The images must be georeferrenced with GPS latitude, longitude, and elevation in 
the EXIF metadata. GCP georeferencing is supported by OpenDroneMap, however it is 
not integrated in our automated pipeline yet.

The full reconstruction can be executed by `poe runall <number-of-power-lines>`, where you 
must supply the number of actual power lines in the field as a parameter.
After the reconstruction is done (usually after several hours), the outputs can be found in
**data/odm/odm_georeferencing/odm_georeferenced_model.laz**. The 3D power line point
cloud will be in **data/power_lines.ply**.

These can be imported into CloudCompare using **File -> Open**. Select *Apply* and then in 
in the *Global shift/scale* dialog, decline the transformation. Next select both point
clouds in the left panel *DB Tree* and choose **Tools -> Distances -> Cloud/Cloud Dist.**
Set **odm_georeferenced_model - Cloud** as *Compared*, and **power_lines - Cloud** as *Reference*.
The output should be the following visualization of distances. You may choose to modify
the color scale according to the power line corridor regulations as in this example:

![Disatance visualization](examples/distances.jpg)

For more visualization options, please refer to the [official documentation](https://www.cloudcompare.org/doc/wiki/). 

## Running Individual Steps
Individual steps of the pipeline can be run using the following poe commands:
- `poe runodm-opensfm [--debug]` -- run the structure from motion part of OpenDroneMap 0pipeline,
 `--debug` shows output
of the OpenDroneMap log,
- `poe runodm-pc [--debug]` -- run the dense reconstruction and rest of the OpenDroneMap pipeline,
- `poe segmentation ` -- run power line segmentation,
- `poe detection <number-of-power-lines>` -- run power line detection,
- `poe reconstruction <number-of-power-lines> [--nocatenary]` -- run power line reconstruction,
 `--nocatenary` will disable catenary curve fitting.
Each step can be rerun as long as the dependencies illustrated in the architecture above are satisfied.

If, for some reason the OpenDroneMap client gets disconnected, it can be reconnected again by running
`poe reconnect-odm [--debug]`.


## Re-train Segmentation Neural Network
We supply trained weights of the neural network for power line segmentation. If for some reason retraining is
necessary, it requires:

- download the modified TTPLA training dataset - `poe download-ttpla-voc`,
- download the CityScapes checkpoint weights `poe download-checkpoint`,
- possibly reconfigure the neural network in **tools/neuralnet_train.py**,
- train the neural network `poe neuralnet-train`,  
- test the neural network `poe neuralnet-test`.

After you are happy with the results, copy the weights of your choice
from **data/segmentation/work_dir** to **data/segmentation/segmentation-weights.pth**.

If the `poe download-ttpla-voc` command fails, please [download the TTPLA dataset
directly from Google Drive](https://drive.google.com/file/d/1tNgwpgBhzVJUhoDlhHQzNuFRjx_Ep6Wu/view?usp=sharing).
Alternatively, the dataset can be generated by downloading the [original TTPLA dataset](https://github.com/R3ab/ttpla_dataset),
converting it to Pascal-VOC format with **tools/dataset_tools/labelme2voc.py**, and removing all non-cable labels
using **tools/dataset_tools/clear-all-but-cables.py**.
## Download Example Datasets
For downloading sample datasets with UAV images, run `poe download-drone-images <dataset-number>`.
Dataset number can be **1, 2 or 3**. Depending on the amount of users, you may get a
Quota Exceeded error from Google Drive. In that case, please re-try again later (possibly the next day), or
[download the datasets directly from Google Drive](https://drive.google.com/drive/folders/1A5omCoTK7Qafml9uX-8i1cEgu7WrOOsY?usp=sharing).

The datasets will be automatically extracted to **data/images**.
