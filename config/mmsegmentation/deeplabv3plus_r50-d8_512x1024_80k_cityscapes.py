"""
Config from the official MMsegmentation repository
(https://github.com/open-mmlab/mmsegmentation/tree/master/configs)
   Copyright 2020 The MMSegmentation Authors.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
_base_ = [
    './_base_/deeplabv3plus_r50-d8.py',
    './_base_/cityscapes.py', './_base_/default_runtime.py',
    './_base_/schedule_80k.py'
]
