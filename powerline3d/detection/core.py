from __future__ import annotations

from operator import itemgetter

import cv2
import numpy as np

import powerline3d.detection.clustering as plclustering
import powerline3d.geometry.planar as plg


def _find_hough_lines(mask: np.ndarray) -> list[plg.LineSegment2D]:
    """
    Probabilistic Hough line detection on a binary segmentation mask
    :param mask: binary segmentation mask
    :return: Hough lines found in the image
    """
    lines = cv2.HoughLinesP(mask, 1, np.pi / 360, 200, None, 200, 100)
    if lines is None:
        return []
    to_line_2d = lambda cv_line: plg.LineSegment2D(np.split(cv_line[0], 2))
    lines = list(map(to_line_2d, lines))
    return lines


def _cluster_lines(lines: [plg.LineSegment2D],
                   im_height: int,
                   im_width: int,
                   v_step: int,
                   cluster_angle_thr: float,
                   cluster_dist_thr: float,
                   v_step_overlap: int) -> plclustering.HorizontalSegments:
    """
    Create horizontal segments, crop Hough lines to each segment, and cluster lines in each segment
    :param lines: Hough lines detected in the image
    :param im_height: image height
    :param im_width: image width
    :param cluster_angle_thr: minimum angle in degrees that separates two power lines
    :param cluster_dist_thr: minimum distance in pixels between two power lines
    :param v_step: horizontal segment height in pixels
    :param v_step_overlap: horizontal segment overlap
    :return horizontal segments, each consisting of its clusters of Hough lines and segment origin
    """
    segments = []
    for offset in range(im_height, -1, -(v_step - int(v_step * v_step_overlap))):
        origin = plg.Point((0, offset - (v_step / 2)))
        cropped_lines = [line.crop_to_roi((0, offset), (im_width, offset - v_step)) for line in lines]
        cropped_lines = list(filter(bool, cropped_lines))
        clustering = plclustering.LineClustering(origin, cluster_angle_thr, cluster_dist_thr)
        segment = clustering.fit(cropped_lines)
        segments.append((segment, origin))
    return segments


def _merge_lines(list_of_hough_lines: list[list[plg.LineSegment2D]]) -> list[plg.LineString]:
    """Merge Hough lines into polylines"""
    merged_lines = [plclustering.merge_lines(pl_hough_lines) for pl_hough_lines in list_of_hough_lines]
    return list(filter(bool, merged_lines))


def _get_cluster_weight(cluster: list[plg.LineSegment2D],
                        power_lines: list[plclustering.PowerLine],
                        step_num: int) -> int:
    """
    Calculate weight of Hough line cluster as number of horizontal segments of the power line that passes through.
    :param cluster: clouster of Hough lines that may appear in some power lines
    :param power_lines: list of detected power lines
    :param step_num: which horizontal segment does the cluster belong to
    :return: score of the cluster
    """
    weight = 0
    for power_line in power_lines:
        p_cluster = power_line.get_cluster(step_num)
        if cluster is p_cluster:
            weight = max(power_line.num_votes, weight)
    return weight


def _filter_line_segments(segments: plclustering.HorizontalSegments,
                          power_lines: [plclustering.PowerLine],
                          num_power_lines: int,
                          min_segments: int) -> plclustering.HorizontalSegments:
    """
    Filter cluster of Hough lines, that appear in "weak" power lines
    :param segments: horizontal segments, each consisting of its clusters of Hough lines and segment origin
    :param power_lines: number of actual power lines
    :param num_power_lines: true number of power lines (to filter only "strongest" num_power_lines * 2 power lines)
    :param min_segments: minimum number of horizontal segments some power line must cover, otherwise, it's
           line clusters are filtered out
    :return: filtered line segments
    """
    filtered_segments = []
    for step_num, (segment, segm_origin) in enumerate(segments):
        weights = [_get_cluster_weight(cluster, power_lines, step_num) for cluster in segment]
        filtered = sorted(zip(segment, weights), key=itemgetter(1), reverse=True)[:num_power_lines * 2]
        filtered = list(map(itemgetter(0), filter(lambda x: x[1] >= min_segments, filtered)))
        filtered_segments.append((filtered, segm_origin))
    return filtered_segments


def detect_power_lines(edge_mask: np.ndarray,
                       num_power_lines,
                       cluster_angle_thr: float = 1,
                       cluster_dist_thr: float = 10,
                       v_step=500,
                       v_step_overlap: int = 0,
                       min_v_steps_filter=4,
                       max_line_gap_steps=4,
                       merge_dist_thr: float = 0,
                       merge_angle_thr: float = 0) -> list[plg.LineString]:
    """
    Detect individual power lines in a power line segmentation mask
    :param edge_mask: grayscale image of edges detected by power line segmentation
    :param num_power_lines: number of power lines in the field
    :param cluster_angle_thr: minimum angle in degrees that separates two power lines
    :param cluster_dist_thr: minimum distance in pixels between two power lines
    :param v_step: horizontal segment height in pixels
    :param v_step_overlap: horizontal segment overlap
    :param min_v_steps_filter: minimum number of horizontal segments the power line must cover
    :param max_line_gap_steps: maximum gap in power line expressed as number of horizontal segments
    :param merge_dist_thr: distance threshold for merging clusters in pixels, default=2*cluster_dist_thr
    :param merge_angle_thr: angle threshold for merging clusters in degrees, default=2*cluster_angle_thr
    :return: a list of detected power lines as polyline objects
    """
    if not merge_angle_thr:
        merge_angle_thr = 2 * cluster_angle_thr
    if not merge_dist_thr:
        merge_dist_thr = 2 * cluster_dist_thr
    lines = _find_hough_lines(edge_mask)
    if len(lines) == 0:
        return []
    lines = [l for l in lines if 45 <= l.angle <= 135]
    clustered_segments = _cluster_lines(lines,
                                        edge_mask.shape[0],
                                        edge_mask.shape[1],
                                        v_step,
                                        cluster_angle_thr,
                                        cluster_dist_thr,
                                        v_step_overlap)
    power_lines = plclustering.merge_clusters(clustered_segments,
                                              merge_dist_thr,
                                              merge_angle_thr,
                                              max_line_gap_steps)
    clustered_segments = _filter_line_segments(clustered_segments, power_lines, num_power_lines, min_v_steps_filter)
    power_lines = plclustering.merge_clusters(clustered_segments,
                                              merge_dist_thr,
                                              merge_angle_thr,
                                              max_line_gap_steps)
    power_lines = _merge_lines([p.all_lines for p in power_lines])
    return power_lines
