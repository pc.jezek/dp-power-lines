from __future__ import annotations

from collections import defaultdict
from operator import itemgetter
from typing import Union, Sequence, TypeVar

import numpy as np
import shapely.geometry as sg
from sklearn.cluster import AgglomerativeClustering

import powerline3d.geometry.planar as plg

T = TypeVar('T')

HorizontalSegments = list[tuple[list[list[plg.LineSegment2D]], plg.Point]]


def hierarchical_clustering(X: np.ndarray, distance_threshold: float, type='average') -> np.ndarray[int]:
    """
    Cluster points using agglomerative clustering, with distance cut at distance_threshold
    :param X: points to cluster
    :param distance_threshold: minimum distance between two clusters
    :param type: linkage type, one of (‘ward’, ‘complete’, ‘average’, ‘single’)
    :return: labels of the clustered points
    """
    if len(X) <= 1:
        return np.arange(len(X))
    clustering = AgglomerativeClustering(n_clusters=None, distance_threshold=distance_threshold, linkage=type).fit(X)
    return clustering.labels_.reshape(len(X))


def _flatten_cluster_labels(nested_cluster_labels: list[np.ndarray]) -> list[int]:
    """
    Flatten labels from two-level (nested) hierarchical clustering
    :param nested_cluster_labels: array containing i arrays of labels, each with label values in {0, ..., k_i}
    :return: flattened array with unique single-level labels
    """
    max_label = -1
    labels = []
    for cluster in nested_cluster_labels:
        labels.extend(cluster + max_label + 1)
        max_label += max(cluster) + 1
    return labels


def group_by_label(items: list[T], labels: list[int]) -> list[list[T]]:
    """
    Arrange items into groups by their labels
    :param items: array if items to group
    :param labels: array of group IDs
    :return: array of arrays of items groupped by id
    """
    num_labels = max(labels) + 1
    items_groupped = [[] for _ in range(num_labels)]
    for line, label in zip(items, labels):
        items_groupped[label].append(line)
    return items_groupped


class DuplicateAddError(Exception):
    pass


class PowerLine:
    """
    Power line as a group of Hough line clusters from horizontal segments
    Segments should be iterated over and sequentially added to this Power Line
    """

    def __init__(self, initial_segment: list[plg.LineSegment2D], detected_step_num: int):
        self.last_detected_step = -1
        self._votes: list[bool] = []
        self._segments: list[list[plg.LineSegment2D]] = []
        self.all_lines: list[plg.LineSegment2D] = []
        self.add_segment(detected_step_num, initial_segment)

    @property
    def num_votes(self) -> int:
        """
        :return: number of horizontal segments where the power line appears
        """
        return sum(self._votes)

    @property
    def last_cluster(self) -> list[plg.LineSegment2D]:
        """
        :return: last detected Hough Line cluster
        """
        return self.get_cluster(self.last_detected_step)

    def has_segment(self, step_num: int) -> bool:
        """
        :return: True if power line was detected at horizontal segment step_num
        """
        if step_num >= len(self._votes):
            return False
        return self._votes[step_num]

    def get_cluster(self, step_num: int) -> list[plg.LineSegment2D]:
        """
        :return: cluster of Hough Lines at horizontal segment step_num
        """
        if step_num > self.last_detected_step:
            return []
        return self._segments[step_num]

    def add_segment(self, step_num: int, segment: list[plg.LineSegment2D]):
        """
        Add horizontal segment to this power line
        :param step_num: segment number
        :param segment: cluster of Hough lines for segment step_num
        """
        if self.has_segment(step_num):
            raise DuplicateAddError
        if self.last_detected_step < step_num:
            num_missing_steps = step_num - self.last_detected_step
            self._votes.extend([False for _ in range(num_missing_steps)])
            self._segments.extend([[] for _ in range(num_missing_steps)])
            self.last_detected_step = step_num
        self._votes[step_num] = True
        self._segments[step_num] = segment
        self.all_lines.extend(segment)


def _find_closest_power_line(cluster: list[plg.LineSegment2D],
                             power_lines: list[PowerLine],
                             segm_origin: plg.Point,
                             step_num: int,
                             max_line_gap_steps=6,
                             discard_threshold=20) -> tuple[int, Union[float, None], Union[float, None]]:
    """
    Find the closest power line to a cluster of Hough Lines based on average distance
    to segment origin
    :param cluster: current Hough Line cluster
    :param power_lines: list of already detected power lines
    :param segm_origin: point of the horizontal segment origin
    :param step_num: current segment number
    :param max_line_gap_steps: maximum allowed distance in pixels between step_num and last power line segment
    :param discard_threshold: discard candidate if any Hough line in the last power line's cluster
           is farther than this distance (in pixels)
    :return: id of closest power line, its distance to segment origin, and average angle difference
    """
    candidates = []
    cluster_dist = np.average([plg.Line2D(line).distance_to_point(segm_origin) for line in cluster])
    cluster_angle = np.average([line.angle for line in cluster])
    for powerline_id, power_line in enumerate(power_lines):
        if step_num - power_line.last_detected_step > max_line_gap_steps:
            continue
        example_line = plg.Line2D(power_line.last_cluster[0])
        example_dist = abs(example_line.distance_to_point(segm_origin) - cluster_dist)
        if example_dist > discard_threshold:
            continue
        line_dists = [abs(plg.Line2D(line).distance_to_point(segm_origin) - cluster_dist) for line in
                      power_line.last_cluster]
        angle_dists = [abs(line.angle - cluster_angle) for line in power_line.last_cluster]
        candidates.append((powerline_id, np.average(line_dists), np.average(angle_dists)))
    if len(candidates) == 0:
        return -1, None, None
    return min(candidates, key=lambda x: x[1])


def merge_clusters(segments: HorizontalSegments,
                   dist_threshold=20,
                   angle_threshold=2,
                   max_line_gap_steps=6) -> list[PowerLine]:
    """
    Merge clusters of horizontal segments vertically -> into distinct power lines
    The result of this function is a list of power lines, each containing all Hough lines that belong to it
    Each power line will also be scored with a number of votes, i.e., number of horizontal segments where it appears
    :param segments: all horizontal segments, each consisting of its clusters of Hough lines and segment origin
    :param dist_threshold: maximum (average) distance in pixels to merge two clusters of Hough lines
    :param angle_threshold: maximum angle in degrees to merge two clusters of Hough lines
    :param max_line_gap_steps: maximum gap of in number of horizontal steps to assign a cluster to a power line
    :return: list of distinct power lines with clusters of Hough lines that belong to them
    """
    if len(segments) == 0:
        return []

    step_num = 0
    first_segment = list(segments[0][0])
    power_lines = [PowerLine(cluster, step_num) for cluster in first_segment]

    for segment, segm_origin in segments[1:]:
        step_num += 1
        matched_clusters = defaultdict(list)
        unmatched_clusters = []

        for cluster in segment:
            closest_id, dist, angle_dist = _find_closest_power_line(
                cluster,
                power_lines,
                segm_origin,
                step_num,
                max_line_gap_steps,
                discard_threshold=3 * dist_threshold
            )
            if closest_id >= 0 and dist < dist_threshold and angle_dist < angle_threshold:
                matched_clusters[closest_id].append((cluster, dist))
            else:
                unmatched_clusters.append(cluster)
        for power_line_id, closest_clusters in matched_clusters.items():
            closest_cluster = min(closest_clusters, key=itemgetter(1))
            power_lines[power_line_id].add_segment(step_num, closest_cluster[0])
        for unmatched_cluster in unmatched_clusters:
            power_lines.append(PowerLine(unmatched_cluster, step_num))

    return power_lines


def merge_lines(lines: list[plg.LineSegment2D], sampling_dist=10, ransac_stop_sample_num=500) -> plg.LineString:
    """
    Merge Hough lines into a single polyline by fitting RANSAC lines
    :param lines: Hough lines to merge
    :param sampling_dist: subsampling resolution in pixels (one point every sampling_dist px) for each line
    :param ransac_stop_sample_num: stop iteration if at least this number of inliers are found.
    :return: polyline LineString (empty if unsuccessful)
    """
    line_points = []
    for line in lines:
        line_points.extend([np.array(p.coords[0]) for p in line.sample_by_dist(sampling_dist)])
    points = np.vstack(line_points)

    if (len(points)) <= 2:
        return plg.LineString()

    line = plg.Line2D(plg.fit_ransac_line(points, stop_sample_num=ransac_stop_sample_num)[0])
    segments = plg.slice_points_along_line(points, line)

    lines = []
    for i, segment in enumerate(segments):
        if len(segment) > 10:
            ransac_line, _ = plg.fit_ransac_line(np.vstack(segment))
            lines.append(ransac_line)

    if len(lines) == 0:
        return plg.LineString()
    prev_line = lines[0]
    result_line_points = [prev_line.p1]
    for next_line in lines[1:]:
        if next_line.distance(prev_line) > 100:
            result_line_points.extend([prev_line.p2, next_line.p1])
        else:
            midpoint = (np.array(prev_line.p2.coords[0]) + np.array(next_line.p1.coords[0])) / 2
            result_line_points.append(sg.Point(midpoint))
        prev_line = next_line
    result_line_points.append(prev_line.p2)
    return plg.LineString(result_line_points)


class LineClustering:
    """
    Two-level hierarchical clustering model:
      - first cluster lines by angle
      - second, cluster lines by distance
    """

    def __init__(self, origin: Union[plg.Point, np.array, Sequence], angle_thr: float = 1, dist_thr: float = 7):
        """
        :param origin: segment origin
        :param angle_thr: minimum average distance in angles between two clusters
        :param dist_thr: minimum average distance in pixels between two clusters
        """
        self.origin = plg.Point(origin)
        self.angle_thr = angle_thr
        self.dist_thr = dist_thr

    def _cluster_lines_by_angle(self, lines: [plg.LineSegment2D]):
        angles = np.array([[line.angle] for line in lines])
        cluster_labels = hierarchical_clustering(angles, self.angle_thr, 'average')
        lines_grouped = group_by_label(lines, cluster_labels.reshape(-1))
        return cluster_labels, lines_grouped

    def _cluster_lines_by_distance(self, lines: [plg.LineSegment2D]):
        if np.isinf(self.dist_thr):
            return np.zeros(len(lines), dtype=int)
        rhos = [[plg.distance_to_line(self.origin, plg.Line2D(segment))] for segment in lines]
        return hierarchical_clustering(np.array(rhos), self.dist_thr, 'average')

    def fit(self, lines: [plg.LineSegment2D]) -> list[list[plg.LineSegment2D]]:
        """
        Cluster lines using agglomerative clustering on two levels:
          - first cluster by angle
          - second, cluster by distance
        :param lines: Hough lines to cluster
        :return: clustered lines grouped by their cluster label
        """
        if len(lines) == 0:
            return []
        _, clustered_lines_by_angle = self._cluster_lines_by_angle(lines)
        nested_clusters = []

        for cluster in clustered_lines_by_angle:
            cluster_labels = self._cluster_lines_by_distance(cluster)
            nested_clusters.append(cluster_labels)

        labels = _flatten_cluster_labels(nested_clusters)
        lines_flat = list(line for cluster in clustered_lines_by_angle for line in cluster)
        return group_by_label(lines_flat, labels)
