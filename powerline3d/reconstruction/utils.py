from __future__ import annotations

import glob
import json
import os.path
from collections import defaultdict
from typing import TextIO

import numpy as np

import powerline3d.utils.draw as cvdraw
from powerline3d.reconstruction import Camera, Shot, PowerLine2D
from powerline3d.utils.calibrate import load_opensfm_calibration


def basename(path: str):
    return os.path.basename(path).rsplit('.')[0]


def load_power_lines(detection_folder: str) -> dict[str, list[PowerLine2D]]:
    power_line_files = glob.glob(os.path.join(detection_folder, '*.json'))
    power_lines_by_shot = defaultdict(list)
    for file in power_line_files:
        pwr_base_name = basename(file)
        with open(file) as pwr_file:
            power_lines_coords = json.load(pwr_file)
        power_lines = [PowerLine2D(coords) for coords in power_lines_coords]
        power_lines_by_shot[pwr_base_name] = power_lines
    return power_lines_by_shot


def load_shots(reconstruction_file: str, detection_folder: str, input_image_folder: str) -> dict[str, Shot]:
    with open(reconstruction_file) as f:
        reconstruction = json.load(f)
        camera = dict(reconstruction[0]['cameras'])
        shots = dict(reconstruction[0]['shots'])
        del reconstruction

    camera = next(iter(camera.values()))
    K, dist = load_opensfm_calibration(camera)
    camera = Camera(camera['width'], camera['height'], K, dist)

    power_lines = load_power_lines(detection_folder)

    return {basename(image_name): Shot(os.path.join(input_image_folder, image_name),
                                       camera,
                                       np.array(shot['rotation']),
                                       np.array(shot['translation']),
                                       power_lines[basename(image_name)])
            for image_name, shot
            in shots.items()}


def load_geo_ref(file_name: str) -> dict[str, str]:
    res = dict()
    with open(file_name) as f:
        res['format'] = f.readline().rstrip('\n')
        res['east'], res['north'] = f.readline().split()
        res['height'] = 0
    return res


def write_ply_point_cloud(file_name: str, points_dict: dict[int, np.ndarray], geo_ref: dict = None):
    if geo_ref is None:
        offset = np.zeros(3, float)
    else:
        offset = np.array([geo_ref['east'], geo_ref['north'], geo_ref['height']], float)

    num_total_points = sum([len(points) for points in points_dict.values()])
    with open(file_name, 'w') as f:
        cmap = cvdraw.get_uniform_cmap(len(points_dict.values()))
        write_ply_header(f, num_total_points)
        for points, color in zip(points_dict.values(), cmap):
            write_ply_points(f, points + offset, color)


def write_ply_header(f: TextIO, num_points: int):
    for line in ['ply',
                 'format ascii 1.0',
                 f'element vertex {num_points}',
                 'property float x',
                 'property float y',
                 'property float z',
                 'property uint8 red',
                 'property uint8 green',
                 'property uint8 blue',
                 'end_header']:
        f.write(line + '\n')


def write_ply_points(f: TextIO, points: np.ndarray, color: tuple[int, int, int]):
    for point in points:
        f.write(f'{point[0]} {point[1]} {point[2]} {color[0]} {color[1]} {color[2]}\n')
