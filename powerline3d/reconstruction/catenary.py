from __future__ import annotations

import warnings

import numpy as np
import scipy.optimize
from scipy.optimize import OptimizeWarning
from sklearn.linear_model import RANSACRegressor

import powerline3d.geometry.planar as plg


def catenary_curve(x, a, b, c):
    """2D catenary curve model"""
    return a + c * np.cosh((x - b) / c)


class _CatenaryModel:
    """Catenary curve model for sklearn RANSACRegressor"""
    initial_guess = [100, 100, 1000]

    def __init__(self):
        self.params = dict()
        self.invalid = False

    def fit(self, X, y):
        """Use scipy.optimize.curve_fit to fit a 2D catenary curve to sample points"""
        try:
            popt, _ = scipy.optimize.curve_fit(catenary_curve, X.reshape(-1), y.reshape(-1), self.initial_guess)
            self.params['a'] = popt[0]
            self.params['b'] = popt[1]
            self.params['c'] = popt[2]
        except:
            self.params['a'] = 0
            self.params['b'] = 0
            self.params['c'] = 1
            self.invalid = True  # if an exception occurred, set model as invalid
        return self

    def score(self, X, y):
        """Mean absolute error"""
        return np.mean(np.abs(self.predict(X) - y))

    def predict(self, X):
        params = [self.params[p] for p in ['a', 'b', 'c']]
        return catenary_curve(X, *params)

    def get_params(self, **kwargs):
        return self.params

    def set_params(self, **params):
        self.params = params


def _initialize_catenary_regressor() -> RANSACRegressor:
    """Initialize RANSACRegressor with the is_model_valid function"""

    def is_model_valid(model, *_args):
        return not model.invalid

    return RANSACRegressor(_CatenaryModel(),
                           min_samples=20,
                           residual_threshold=0.2,
                           is_model_valid=is_model_valid,
                           max_trials=1500)


def disable_warnings(fn):
    """
    It is common to encounter warnings from scipy.optimize, because the sample points may be ill conditioned.
    Disables the warnings for convenience.
    """

    def wrapper(*args, **kwargs):
        warnings.simplefilter('ignore', OptimizeWarning)
        warnings.simplefilter('ignore', RuntimeWarning)

        res = fn(*args, **kwargs)

        warnings.simplefilter('default', OptimizeWarning)
        warnings.simplefilter('default', RuntimeWarning)

        return res

    return wrapper


@disable_warnings
def fit_catenary(points_3d: dict[int, np.ndarray], num_sampled_points=1000) -> dict[int, np.ndarray]:
    """
    Fit 3D catenary curve to 3D points by:
    1. Fitting RANSAC line to the XY coords of the points (ground)
    2. Fitting 2D Catenary curve to XZ or YZ (depending on which spans bigger domain)
    2. Predict Z coordinate from point's X or Y coordinate using the 2D catenary curve model
    :param points_3d: dictionary of key: "Power line ID" and value: "3D points"
    :param num_sampled_points: number of points to sample per each fitted catenary
    :return: dictionary of key "Power line ID" and value: points sampled from fitted catenary curve
    """
    points_3d_cat = dict()
    ransac_regressor = _initialize_catenary_regressor()

    for key, pts in points_3d.items():
        if not len(pts):
            continue
        try:
            line, _ = plg.fit_ransac_line(pts[:, :2], residual_threshold=.5)
            xy = np.vstack([np.array(p.coords[0]) for p in line.sample_points(num_sampled_points)])
            use_y = abs(line.p1.x - line.p2.x) < abs(line.p1.y - line.p2.y)

            if use_y:
                X, y = pts[:, 1].reshape(-1, 1), pts[:, 2].reshape(-1, 1)
                pred_X = xy[:, 1].reshape(-1, 1)
            else:
                X, y = pts[:, 0].reshape(-1, 1), pts[:, 2].reshape(-1, 1)
                pred_X = xy[:, 0].reshape(-1, 1)

            model = ransac_regressor.fit(X, y)
            z = model.predict(pred_X)

            new_pts = np.hstack([xy, z.reshape(-1, 1)])
            points_3d_cat[key] = new_pts[new_pts[:, 2] < np.max(pts[:, 2])]
        except ValueError:
            warnings.warn(f'Could not robustly fit catenary curve to the powerline {key}. Using the original points.')
            points_3d_cat[key] = pts

    return points_3d_cat
