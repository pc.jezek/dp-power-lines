from __future__ import annotations

import numpy as np

import powerline3d.geometry.planar as plg


class PowerLine2D:
    """2D polyline representing power line in an image"""

    def __init__(self, coords: np.ndarray):
        self.coords = coords
        self.line_string = plg.LineString(coords)
