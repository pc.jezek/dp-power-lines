from __future__ import annotations

from dataclasses import dataclass, field
from enum import Enum
from typing import NewType, Union

from powerline3d.reconstruction.models.powerline2d import PowerLine2D
from powerline3d.reconstruction.models.shot import Shot

"""
Three types of power line matches -> simple, transfer or empty
"""


class MatchType(Enum):
    SIMPLE = 'simple'
    TRANSFER = 'transfer'
    EMPTY = 'empty'


@dataclass
class SimpleOrderingMatch:
    type = MatchType.SIMPLE

    # stereo pair
    first: Shot
    second: Shot

    # matching power lines (ordered)
    first_power_lines: [PowerLine2D] = field(default_factory=list)
    second_power_lines: [PowerLine2D] = field(default_factory=list)


@dataclass
class TransferPointsMatch:
    type = MatchType.TRANSFER

    # stereo pair
    first: Shot
    second: Shot

    # matching power lines (ordered)
    first_power_lines: [PowerLine2D]
    second_power_lines: [PowerLine2D]


class EmptyOrderingMatch:
    type = MatchType.EMPTY


ParallelShotMatch = NewType('ParallelShotMatch', Union[SimpleOrderingMatch, EmptyOrderingMatch, TransferPointsMatch])
