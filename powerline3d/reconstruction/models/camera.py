from __future__ import annotations


class Camera:
    """
    Contains info about camera, mainly intrinsic camera parameters -- K, and distortion coefficients
    """

    def __init__(self, width, height, camera_matrix, distortion_coefficients):
        self.width = int(width)
        self.height = int(height)
        self.K = camera_matrix
        self.dist = distortion_coefficients
