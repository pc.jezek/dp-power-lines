from __future__ import annotations

import os.path

import cv2
import numpy as np

import powerline3d.geometry.epipolar as epg
import powerline3d.geometry.planar as plg
from powerline3d.reconstruction.models.camera import Camera
from powerline3d.reconstruction.models.powerline2d import PowerLine2D


class Shot:
    """
    Represents the geometry of a single image, with its intrinsic and extrinsic parameters
    Contains methods for calculating relationships with other images using epipolar geometry
    """

    def __init__(self,
                 image_path: str,
                 camera: Camera,
                 rotation: np.ndarray,
                 translation: np.ndarray,
                 power_lines: list[PowerLine2D] = None):
        self.image_path = image_path
        self.image_name = os.path.basename(image_path)
        self.camera = camera
        self.R = cv2.Rodrigues(rotation)[0]
        self.t = np.array(translation)
        self.power_lines: list[PowerLine2D] = power_lines if power_lines else []

        # Projection matrix
        self.P = epg.get_projection_matrix(camera.K, self.R, self.t)

        self.optical_center_coords_w = self.get_world_coordinates(np.zeros(3))

    def get_world_coordinates(self, point_in_camera_coordinates: np.ndarray) -> np.ndarray:
        """ Translate point from camera coordinates of this shot to the world coordinates """
        return self.R.T.dot(point_in_camera_coordinates - self.t)

    def get_camera_coordinates(self, point_in_world_coordinates) -> np.ndarray:
        """ Translate point from the world coordinates to camera coordinates of this shot """
        return self.R.dot(point_in_world_coordinates) + self.t

    def get_relative_rotation_translation(self, other: Shot) -> tuple[np.ndarray, np.ndarray]:
        """
        Returns relative rotation and translation matrices assuming *self* to be at origin of the coordinates
        https://math.stackexchange.com/questions/709622/relative-camera-matrix-pose-from-global-camera-matrixes
        """
        R = other.R.dot(self.R.T)
        t = -R.dot(self.t) + other.t
        return R, t

    def get_fundamental_matrix(self, other, normalize=True) -> np.ndarray:
        """Return fundamental matrix describing pixel relationships between self and other image"""
        R, t = self.get_relative_rotation_translation(other)
        return epg.get_fundamental_matrix(self.camera.K, R, t, normalize)

    def find_epipolar_lines(self, other, points) -> list[plg.LineSegment2D]:
        """
        Find epipolar lines in other shot given points in this shot
        :param other: shot where the lines should be found
        :param points: pixel coordinates in this shot (self)
        :return: epipolar lines in other shot corresponding to points
        """
        h, w = self.camera.height, self.camera.width
        F = self.get_fundamental_matrix(other)
        lines = cv2.computeCorrespondEpilines(points.reshape(-1, 1, 2), 1, F)
        lines = lines.reshape(-1, 3)
        res = []
        for h in lines:
            x0, y0 = map(int, [0, -h[2] / h[1]])
            x1, y1 = map(int, [w, -(h[2] + h[0] * w) / h[1]])
            res.append(plg.LineSegment2D([(x0, y0), (x1, y1)]))
        return res

    def project_w_points(self, points: np.ndarray) -> np.ndarray:
        """
        Project 3D points in object (world) coordinates into this shot
        :param points: 3D points to project to this shot
        :return: 2D point projections
        """
        image_points = []
        for p in points:
            p2D = self.P @ np.hstack([p, [1]])
            p2D /= p2D[2]
            image_points.append(p2D[:2])
        return np.array(image_points)
