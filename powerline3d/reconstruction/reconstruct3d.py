from __future__ import annotations

from collections import defaultdict
from copy import copy
from operator import itemgetter
from typing import Union

import cv2
import numpy as np

import powerline3d.geometry.epipolar as epg
import powerline3d.geometry.planar as plg
from powerline3d.reconstruction.models import (ParallelShotMatch, EmptyOrderingMatch, MatchType, SimpleOrderingMatch,
                                               TransferPointsMatch, PowerLine2D, Shot)
from powerline3d.reconstruction.sorting import ParallelShotGroup


def reconstruct_3d_power_line(first_shot: Shot,
                              second_shot: Shot,
                              pwr_1: PowerLine2D,
                              pwr_2: PowerLine2D,
                              num_points=50) -> np.ndarray:
    """
    Reconstructs 3D points of a single power line captured in two images (a stereo pair)
    :param first_shot: left shot from a stereo pair
    :param second_shot: right shot from a stereo pair
    :param pwr_1: power line in first_shot
    :param pwr_2: corresponding power line in second_shot
    :param num_points: how many points to sample and reconstruct
    :return: reconstructed 3D points of the power line
    """
    points = plg.sample_points(pwr_1.line_string, num_points)
    points = np.array([np.array(p.coords[0]) for p in points])
    epipolar_lines = first_shot.find_epipolar_lines(second_shot, points)
    intersections = [epipolar_line.intersection(pwr_2.line_string) for epipolar_line in epipolar_lines]
    intersections = [(point, np.array(intersection.coords[0])) for point, intersection in
                     zip(points, intersections)
                     if
                     not intersection.is_empty]
    if len(intersections) == 0:
        return np.array([])
    pts1, pts2 = map(np.array, zip(*intersections))
    return epg.reconstruct_3d_points(first_shot.P, second_shot.P, pts1, pts2)


def _get_simple_ordering_intersections(shot: Shot, n_power_lines: int) -> Union[None, tuple[list[plg.Point],
                                                                                            list[plg.Point],
                                                                                            list[plg.Point]]]:
    """Get intersections with three lines (top, center, bottom) at (15%, 50%, and 85% of the image height)"""
    if len(shot.power_lines) != n_power_lines:
        return None

    h, w = shot.camera.height, shot.camera.width
    offset_from_edge = int(h * 0.15)  # 15 percent
    top_line = plg.LineSegment2D([(0, offset_from_edge), (w, offset_from_edge)])
    mid_line = plg.LineSegment2D([(0, h // 2), (w, h // 2)])
    bot_line = plg.LineSegment2D([(0, h - offset_from_edge), (w, h - offset_from_edge)])

    intersections = ([top_line.intersection(pwr.line_string) for pwr in shot.power_lines],
                     [mid_line.intersection(pwr.line_string) for pwr in shot.power_lines],
                     [bot_line.intersection(pwr.line_string) for pwr in shot.power_lines])
    return intersections


def _find_closest_power_line(points_3d: np.ndarray,
                             shot: Shot,
                             dist_thr=20,
                             min_votes=20) -> tuple[bool, Union[PowerLine2D, None]]:
    """
    Project 3D points into the given shot, and find the power line that aligns with the projections (is closest)
    :param points_3d: points of a power line in the object coordinates
    :param shot: contains the candidate power lines
    :param dist_thr: max distance between point projection and the power line candidate
    :param min_votes: minimum number of points that fall within dist_thr of the closest power line
    :return: closest 2D power line that is most likely the projection of points_3d
    """
    votes = defaultdict(int)
    points = []
    for point_3d in points_3d:
        [point_2d] = shot.project_w_points(np.array([point_3d]))
        points.append(point_2d)
        point_2d = plg.Point(point_2d)
        dists = [pwr.line_string.distance(point_2d) for pwr in shot.power_lines]
        min_dist = np.min(dists)
        if min_dist < dist_thr:
            votes[np.argmin(dists)] += 1
    sorted_votes = list(sorted(votes.items(), key=itemgetter(1), reverse=True))
    if sorted_votes and sorted_votes[0][1] > min_votes:
        return True, shot.power_lines[sorted_votes[0][0]]
    return False, None


def _check_order_consistency(*point_arrays, min_nonempty_rows=2) -> Union[np.ndarray, None]:
    """
    Checks if the order of point intersections with multiple horizontal lines is consistent for each line
    :param point_arrays: Array of rows, each row containing ,
    :param min_nonempty_rows: the minimum number of rows that must have all intersections with a corresponding line
    :return: the indices that would sort the columns left to right, None if not consistent
    """
    if len(point_arrays) < min_nonempty_rows:
        return None
    sorted_pts = np.vstack([np.argsort([p.x for p in points]) for points in point_arrays])

    if (sorted_pts == sorted_pts[0]).all():
        return sorted_pts[0]
    else:
        return None


def _match_by_simple_ordering(group: ParallelShotGroup, n_power_lines: int) -> ParallelShotMatch:
    """
    Match power lines in a parallel group by:
    1. draw three lines across all images and check if the power lines register exactly n_power_lines intersection
       with all three lines
    2. if at least two images satisfy this condition, check if the order of the intersections with the power lines is
       consistent for all three lines
    3. if so, sort the power lines left-to-right, and the corresponding pairs as "simple matched"
    :param group: parallel left, middle, and right images
    :param n_power_lines: number of power lines in the field
    :return: SimpleOrderingMatch if the matching succeeded, EmptyOrderingMatch otherwise
    """
    shots = (group.left, group.right, group.middle)
    intersections = [_get_simple_ordering_intersections(shot, n_power_lines) for shot in shots]
    shot_candidates = [shot for i, shot in enumerate(shots) if intersections[i]]

    intersections = list(filter(bool, intersections))
    intersections_filtered = []
    for intersection_arrays in intersections:
        intersections_filtered.append([points for points in intersection_arrays if
                                       not any([p.is_empty for p in points])])
    intersections = intersections_filtered
    stuff_sorted = list(sorted([(s, i) for s, i in zip(shot_candidates, intersections)],
                               key=lambda x: len(x[1]), reverse=True))
    shot_candidates = [s for s, _ in stuff_sorted]
    intersections = [i for _, i in stuff_sorted]

    if len(shot_candidates) < 2:
        return EmptyOrderingMatch()

    main_shot, other_shot = shot_candidates[0], shot_candidates[1]
    main_intersections, other_intersections = intersections[0], intersections[1]

    main_argsort = _check_order_consistency(*main_intersections)
    other_argsort = _check_order_consistency(*other_intersections)

    if main_argsort is not None and other_argsort is not None:
        R, _ = main_shot.get_relative_rotation_translation(other_shot)
        z_rotation = cv2.Rodrigues(R)[0].reshape(3)[2]
        if abs(z_rotation) > np.pi / 2:
            other_argsort = list(reversed(other_argsort))
        main_power_lines = [main_shot.power_lines[i] for i in main_argsort]
        other_power_lines = [other_shot.power_lines[i] for i in other_argsort]
        return SimpleOrderingMatch(main_shot, other_shot, main_power_lines, other_power_lines)

    return EmptyOrderingMatch()


def _merge_matches(forward_matches: ParallelShotMatch, backward_matches: ParallelShotMatch) -> list[ParallelShotMatch]:
    """Merge forward and backward matches"""
    assert len(forward_matches) == len(backward_matches)
    matches = []
    for fwd, back in zip(forward_matches, reversed(backward_matches)):
        if fwd.type == MatchType.EMPTY or back.type == MatchType.SIMPLE:
            matches.append(back)
            continue
        elif back.type == MatchType.EMPTY or fwd.type == MatchType.SIMPLE:
            matches.append(fwd)
            continue

        assert fwd.first == back.first
        assert fwd.second == back.second

        first_power_lines = copy(fwd.first_power_lines)
        second_power_lines = copy(fwd.second_power_lines)
        for power_line_first, power_line_second in zip(back.first_power_lines, back.second_power_lines):
            if power_line_first not in fwd.first_power_lines and power_line_second not in fwd.second_power_lines:
                first_power_lines.append(power_line_first)
                second_power_lines.append(power_line_second)
        matches.append(TransferPointsMatch(fwd.first, fwd.second, first_power_lines, second_power_lines))
    return matches


def _filter_power_lines(shot_groups: [ParallelShotGroup],
                        power_line_points: dict[int, np.ndarray]) -> dict[int, np.ndarray]:
    """Filter obvious outliers (power lines should never be too much above or below the shots origin centers)"""
    filtered_power_lines = dict()
    sample_shot_height = shot_groups[0].left.optical_center_coords_w[2]
    min_height = sample_shot_height - 150  # flying height should never be more than 150m above power lines

    for id, points in power_line_points.items():
        below_surface = np.sum(points[:, 2] < min_height)
        above_cameras = np.sum(points[:, 2] > sample_shot_height)
        if below_surface + above_cameras < len(points) * 0.1:
            filtered_power_lines[id] = np.array([p for p in points if min_height <= p[2] <= sample_shot_height])
    return filtered_power_lines


class PowerLineReconstructionModel:
    def __init__(self, n_power_lines: int):
        self.n_power_lines = n_power_lines
        self._parallel_shot_matches: list[ParallelShotMatch] = []
        self._shot_groups: list[ParallelShotGroup] = []
        self._power_line_id_gen = PowerLineReconstructionModel._powerline_id_gen_fn()
        self._power_line_id_map: dict[PowerLine2D, int] = defaultdict(int)
        self._power_line_3d_points: dict[int, [np.ndarray]] = defaultdict(list)

    @staticmethod
    def _powerline_id_gen_fn():
        """Power line ID generator (starting at 1)"""
        pwr_id = 1
        while True:
            yield pwr_id
            pwr_id += 1

    def _reset(self):
        """Reset model into initial state"""
        self._parallel_shot_matches = []
        self._shot_groups = []
        self._power_line_id_gen = PowerLineReconstructionModel._powerline_id_gen_fn()
        self._power_line_id_map = defaultdict(set)
        self._power_line_points = defaultdict(list)

    def _unify_power_lines(self, current_power_lines: list[PowerLine2D], next_power_lines: list[PowerLine2D]):
        """
        Assign labels from current_power_lines to next_power_lines (in the same order). If some power line
        in current_power_lines has no ID, it is generated
        :param current_power_lines: source of power line IDs
        :param next_power_lines: target for power line IDs
        :return:
        """
        for p in current_power_lines:
            if not self._power_line_id_map[p]:
                self._power_line_id_map[p] = next(self._power_line_id_gen)
        for cur_pl, next_pl in zip(current_power_lines, next_power_lines):
            if not self._power_line_id_map[next_pl]:
                self._power_line_id_map[next_pl] = self._power_line_id_map[cur_pl]

    def _match_next_group(self,
                          next_group: ParallelShotGroup,
                          current_match: ParallelShotMatch,
                          next_match: ParallelShotMatch) -> ParallelShotMatch:
        """
        Match power lines by transfer matching if they were not simply matched.
        Transfer matching requires a matched pair of power lines. It first reconstructs 3D points
        from them, and then projects them to the next group of images, where it looks for a match.
        For both transfer and simple matching, the power line IDs are carried to the next group
        (only if the next group doesn't already contain IDs)
        :param next_group: power lines in this group will be identified with IDs from current_match
        :param current_match: stereo pair that helps with identifying next_match
        :param next_match: stereo pair to be matched
        :return: next group match after potential Transfer matching
        """
        if current_match.type == MatchType.EMPTY:
            return next_match

        if current_match.type == MatchType.SIMPLE and next_match.type == MatchType.SIMPLE:
            self._unify_power_lines(current_match.first_power_lines, next_match.first_power_lines)
            return next_match
        first_power_lines, second_power_lines = [], []

        for pwr_1, pwr_2 in zip(current_match.first_power_lines, current_match.second_power_lines):
            points_3d = reconstruct_3d_power_line(current_match.first, current_match.second, pwr_1, pwr_2)
            res_left, closest_power_line_left = _find_closest_power_line(points_3d, next_group.left)
            res_right, closest_power_line_right = _find_closest_power_line(points_3d, next_group.right)
            if res_left and res_right:
                first_power_lines.append(closest_power_line_left)
                second_power_lines.append(closest_power_line_right)
                self._unify_power_lines([pwr_1], [closest_power_line_left])
        if len(first_power_lines) > 0:
            if next_match.type != 'simple':
                return TransferPointsMatch(next_group.left, next_group.right, first_power_lines, second_power_lines)
        return next_match

    def _transfer_points_matching_forward(self) -> list[ParallelShotMatch]:
        matches = [self._parallel_shot_matches[0]]
        current_match = self._parallel_shot_matches[0]
        for idx in range(len(self._shot_groups) - 1):
            next_group = self._shot_groups[idx + 1]
            next_match = self._parallel_shot_matches[idx + 1]
            current_match = self._match_next_group(next_group, current_match, next_match)
            matches.append(current_match)
        return matches

    def _transfer_points_matching_backward(self) -> list[ParallelShotMatch]:
        matches = [self._parallel_shot_matches[-1]]
        current_match = self._parallel_shot_matches[-1]
        for idx in range(len(self._shot_groups) - 1, 0, -1):
            next_group = self._shot_groups[idx - 1]
            next_match = self._parallel_shot_matches[idx - 1]
            current_match = self._match_next_group(next_group, current_match, next_match)
            matches.append(current_match)
        return matches

    def _reconstruct_all_power_lines(self, matches: list[ParallelShotMatch]) -> dict[int, np.ndarray]:
        """Get 3D points for all power lines from the consecutive stereo matches"""

        points_3d: dict[int, list] = defaultdict(list)
        for match in matches:
            if match.type == MatchType.EMPTY:
                continue
            for pwr_1, pwr_2 in zip(match.first_power_lines, match.second_power_lines):
                points_3d_pwr = reconstruct_3d_power_line(match.first, match.second, pwr_1, pwr_2)
                power_line_id = self._power_line_id_map[pwr_1]
                if power_line_id is None:
                    power_line_id = next(self._power_line_id_gen)
                points_3d[power_line_id].extend(points_3d_pwr)

        points_3d_res: dict[int, np.ndarray] = dict()
        for k, v in points_3d.items():
            points_3d_res[k] = np.vstack(v)

        return points_3d_res

    def fit(self, shot_groups: [ParallelShotGroup]) -> dict[int, np.ndarray]:
        """
        Power line 3D reconstruction
        :param shot_groups: ordered consecutive groups of three parallel images (left, middle, right)
        :return: dictionary of key "Power line ID" and value: 3D points reconstructed from 2D power lines
        """
        self._reset()

        self._shot_groups = shot_groups
        self._parallel_shot_matches = [_match_by_simple_ordering(group, self.n_power_lines) for group in shot_groups]

        forward_matches = self._transfer_points_matching_forward()
        backward_matches = self._transfer_points_matching_backward()

        matches = _merge_matches(forward_matches, backward_matches)

        points_3d = self._reconstruct_all_power_lines(matches)
        points_3d = _filter_power_lines(shot_groups, points_3d)

        return points_3d
