from __future__ import annotations

import numpy as np

from powerline3d.reconstruction import fit_catenary
from powerline3d.reconstruction.models import Shot
from powerline3d.reconstruction.reconstruct3d import PowerLineReconstructionModel
from powerline3d.reconstruction.sorting import sort_shots


def reconstruct_power_lines(shots: [Shot], num_power_lines: int, disable_catenary=False) -> dict[int, np.ndarray]:
    """
    Reconstruct 3D points from 2D power lines detected in shots.
    :param shots: information about images, containing intrinsic, extrinsic parameters and 2D detected power lines
    :param num_power_lines: number of power lines in the field
    :param disable_catenary: no catenary curve fitting
    :return: dictionary of key "Power line ID" and value: 3D points of the corresponding power line
    """
    shot_groups = sort_shots(shots)
    model = PowerLineReconstructionModel(num_power_lines)
    power_lines_3d = model.fit(shot_groups)
    if not disable_catenary:
        power_lines_3d = fit_catenary(power_lines_3d)
    return power_lines_3d
