from __future__ import annotations

from dataclasses import dataclass
from operator import itemgetter

import numpy as np
from sklearn.cluster import AgglomerativeClustering

import powerline3d.geometry.planar as plg
from powerline3d.detection.clustering import group_by_label
from powerline3d.reconstruction import Shot


def _fit_line_string_to_centers(shots: [Shot]) -> plg.LineString:
    """Fit polyline through XY coordinates (ground) of the optical centers of all the shots"""
    optical_centers_w_2d = np.array([shot.optical_center_coords_w[:2] for shot in shots])
    return plg.fit_line_string(optical_centers_w_2d)


def _cluster_shots(shots: [Shot],
                   center_line_string: plg.LineString,
                   n_strips=3) -> tuple[list[list[Shot]], list[list[float]]]:
    """
    Cluster the shots into flight strips based on signed distance to center_line_string
    :param shots: to be clustered
    :param center_line_string: 2D polyline fitted to the optical centers
    :param n_strips: number of flight strips
    :return: shots and their distances grouped by labels
    """
    shots_2d = np.array([shot.optical_center_coords_w[:2] for shot in shots])
    distances = [plg.signed_distance_to_line_string(plg.Point(point), center_line_string) for point in shots_2d]
    X = np.array(distances).reshape(-1, 1)
    labels = AgglomerativeClustering(n_clusters=n_strips, linkage='average').fit(X).labels_
    return group_by_label(shots, labels), group_by_label(distances, labels)


def get_flight_strips(shots: [Shot], center_line_string: plg.LineString) -> tuple[list[Shot], list[Shot], list[Shot]]:
    """
    :param center_line_string: 2D polyline fitted to the optical centers
    Assign shots into left, middle, and right flight strips
    """
    shot_strips, shot_distances = _cluster_shots(shots, center_line_string)
    dist_averages = [np.average(np.abs(dists)) for dists in shot_distances]
    dist_averages_sorted = list(reversed(np.argsort(dist_averages)))

    left_strip = shot_strips[dist_averages_sorted[0]]
    right_strip = shot_strips[dist_averages_sorted[1]]
    middle_strip = shot_strips[dist_averages_sorted[2]] if len(shot_strips) > 2 else []

    return left_strip, middle_strip, right_strip


def _project_shots(center_line_string: plg.LineString, shots: [Shot]) -> np.ndarray:
    """Project shots XY coordinates to 2D line"""
    points = (plg.Point(shot.optical_center_coords_w[:2]) for shot in shots)
    return np.array([center_line_string.project(point, normalized=True) for point in points])


def _find_nearest_sorted(item: float, sorted_array: np.ndarray) -> float:
    """Find nearest item in sorted array using binary search"""
    assert (len(sorted_array) > 0)
    insert_idx = np.searchsorted(sorted_array, item)

    if insert_idx - 1 == 0:
        return insert_idx
    if insert_idx >= len(sorted_array):
        return insert_idx - 1

    indices = [insert_idx - 1, insert_idx]
    closest = np.argmin(np.abs(sorted_array[indices] - item))
    return indices[closest]


def group_parallel_shots(center_line_string: plg.LineString,
                         left_strip: [Shot],
                         middle_strip: [Shot],
                         right_strip: [Shot]) -> list[ParallelShotGroup]:
    """
    Choose groups of three parallel images (left, middle, right) from flight strips, based on projections
    to center_line_string
    :param center_line_string: reference 2D line
    :param left_strip: flight strip left of power lines
    :param middle_strip: flight strip above power lines
    :param right_strip: flight strip right of power lines
    :return: consecutive groups of (left, middle, right) images (one image can rarely be in multiple groups)
    """

    def _sort_strip(center_line_string: plg.LineString, strip):
        projections = _project_shots(center_line_string, strip)
        return list(sorted(zip(strip, projections), key=itemgetter(1)))

    strips = [left_strip, middle_strip, right_strip]
    left_strip, middle_strip, right_strip = [_sort_strip(center_line_string, strip) for strip in strips]
    middle_strip_dists = np.array([dist for _, dist in middle_strip])
    right_strip_dists = np.array([dist for _, dist in right_strip])

    groups = []
    for left, dist in left_strip:
        middle = middle_strip[_find_nearest_sorted(dist, middle_strip_dists)][0]
        right = right_strip[_find_nearest_sorted(dist, right_strip_dists)][0]
        groups.append(ParallelShotGroup(left, middle, right))
    return groups


@dataclass
class ParallelShotGroup:
    left: Shot
    middle: Shot
    right: Shot


def sort_shots(shots: [Shot]) -> list[ParallelShotGroup]:
    """
    Sort shops into consecutive groups of three parallel images (left, middle, right)
    :param shots: all shots
    :return: list of consecutive groups of parallel images (left, middle, right)
    """
    center_line_string = _fit_line_string_to_centers(shots)
    flight_strips = get_flight_strips(shots, center_line_string)
    return group_parallel_shots(center_line_string, *flight_strips)
