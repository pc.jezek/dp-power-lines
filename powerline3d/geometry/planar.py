from __future__ import annotations

from operator import itemgetter
from typing import Union, Collection

import numpy as np
from shapely.geometry import Point, box, LineString
from skimage.measure import LineModelND, ransac


def get_line_angle(p1: Union[Point, tuple], p2: Union[Point, tuple]) -> float:
    """
    Calculate positive angle of the line with the x-axis
    """
    p1, p2 = Point(p1), Point(p2)
    if p2.y > p1.y:
        u = (p2.y - p1.y), (p2.x - p1.x)
    else:
        u = (p1.y - p2.y), (p1.x - p2.x)
    return float(np.degrees(np.arctan2(*u)))


def _sample(line_or_line_string: Union[LineSegment2D, Line2D], num_points: int, sampling_dist: float) -> list[Point]:
    return [line_or_line_string.interpolate(i * sampling_dist) for i in range(num_points)]


def sample_by_dist(line_or_line_string: Union[LineSegment2D, Line2D], sampling_dist: int) -> list[Point]:
    """Sample points from Line2D or LineString by distance along line"""
    num_points = int(np.round(line_or_line_string.length / sampling_dist + 1))
    return _sample(line_or_line_string, num_points, sampling_dist)


def sample_points(line_or_line_string: Union[LineSegment2D, Line2D], num_points) -> list[Point]:
    """Sample num_points equidistant points from Line2D or LineString"""
    sampling_dist = line_or_line_string.length / num_points
    return _sample(line_or_line_string, num_points, sampling_dist)


class LineSegment2D:
    """
    LineSegment consisting of two points.
    Using LineString from shapely as implementation -> all methods regarding geometry are forwarded to it
    """

    def __init__(self, coords_or_line_segment):
        if isinstance(coords_or_line_segment, self.__class__):
            coords = coords_or_line_segment.coords
        else:
            coords = coords_or_line_segment
        assert len(coords) == 2
        self.line_string = LineString(coords)

        self.p1, self.p2 = map(Point, coords)
        self.angle = get_line_angle(self.p1, self.p2)

    def crop_to_roi(self,
                    roi_corner1: Union[Point, Collection[int]],
                    roi_corner2: Union[Point, Collection[int]]) -> LineSegment2D:
        """
        Crop LineString to a rectangular region given by roi_corner1, roi_corner2
        :param roi_corner1: any rectangle corner, e.g., top left
        :param roi_corner2: opposite (furthest) corner, e.g. bottom right
        :return: Cropped LineSegment2D
        """
        roi_corner1, roi_corner2 = Point(roi_corner1), Point(roi_corner2)
        min_x, max_x = sorted((roi_corner1.x, roi_corner2.x))
        min_y, max_y = sorted((roi_corner1.y, roi_corner2.y))
        roi_box = box(min_x, min_y, max_x, max_y)
        intersection = self.intersection(roi_box).coords
        return LineSegment2D(intersection) if len(intersection) > 1 else None

    def sample_by_dist(self, sampling_dist):
        return sample_by_dist(self, sampling_dist)

    def sample_points(self, num_points):
        return sample_points(self, num_points)

    def __getattr__(self, name):
        """Forward all other operations to LineString"""
        return getattr(self.line_string, name)


class Line2D:
    """
    LineSegment extended to infinite line
    """

    def __init__(self, coords_or_line_segment):
        self.line_segment = LineSegment2D(coords_or_line_segment)
        self.origin = Point(self.line_segment.p1)
        self.angle = self.line_segment.angle
        self.dist_to_origin = self.distance_to_point(Point((0, 0)))

    def distance_to_point(self, point: Point):
        return distance_to_line(point, self)


def slice_points_along_line(points: np.ndarray,
                            line: Line2D,
                            segment_size=200) -> list[list[np.ndarray]]:
    """
    Orthogonally project points to the line, sort them, and group them into buckets every segment_size units
    :param points: list of points scattered around line
    :param line: reference line
    :param segment_size:
    :return: groups of points
    """
    angle = np.radians(line.angle)
    c, s = np.cos(angle), np.sin(angle)
    rotation_matrix = np.array(((c, -s), (s, c)))
    rotated_points = points @ rotation_matrix
    min_x, max_x = min(rotated_points[:, 0]), max(rotated_points[:, 0])
    n_segments = int((max_x - min_x) / segment_size)
    indices = np.digitize(rotated_points[:, 0], bins=(np.linspace(min_x, max_x + 1, n_segments + 1)))
    segments = [[] for _ in range(n_segments + 1)]
    for point, segment_idx in zip(points, indices):
        segments[segment_idx - 1].append(point)
    return segments


def distance_to_line(p: Point, line: Line2D):
    """
     Got this from https://stackoverflow.com/a/1501725


                      p
                    + |+
                  +   | +
                +    d|  +
              +       |   +
             u--------P----v

     P: projection of point p on line segment uv
     t: number between [0,1] expressing the ratio of:
         distance of P from u / length of |uv|

     """

    v, w = line.line_segment.p1, line.line_segment.p2
    l2 = v.distance(w) ** 2
    t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2
    projection = Point(v.x + t * (w.x - v.x), v.y + t * (w.y - v.y))
    return p.distance(projection)


def signed_distance_to_line_string(p: Point, line_string: LineString) -> float:
    """
    Compute distance from line_string to point with a sign
    (+1 if the point is on the "left" of the line string, else -1)

    Assumes that the given linestring is almost a line, otherwise, sign might not be accurate
    https://stackoverflow.com/questions/1560492/how-to-tell-whether-a-point-is-to-the-right-or-left-side-of-a-line
    """
    offset = line_string.project(p)
    offset_2 = line_string.project(Point(np.array(p.coords[0]) + line_string.length / 100))
    [point_on_line] = np.array(line_string.interpolate(offset).coords)
    [point_on_line_2] = np.array(line_string.interpolate(offset_2).coords)
    ax, ay = point_on_line
    bx, by = point_on_line_2
    cx, cy = np.array(p.coords[0])
    sign = 1 if (bx - ax) * (cy - ay) - (by - ay) * (cx - ax) > 0 else -1
    return line_string.distance(p) * sign


def fit_ransac_line(points: np.ndarray,
                    min_samples=2,
                    stop_sample_num=500,
                    residual_threshold=10) -> tuple[LineSegment2D, np.ndarray]:
    """
    Fit line through points using RANSAC algorithm
    :param points: numpy array of N 2D points of shape (N, 2)
    :param min_samples: the minimum number of data points to fit a model to.
    :param stop_sample_num: stop iteration if at least this number of inliers are found.
    :param residual_threshold: maximum distance for a data point to be classified as an inlier.
    :return: line cropped to the area occupied by the points
    """

    model_robust, inliers = ransac(points, LineModelND, min_samples=min_samples,
                                   stop_sample_num=stop_sample_num,
                                   residual_threshold=residual_threshold)
    direction = LineSegment2D([[0, 0], (model_robust.params[1])]).angle
    is_vertical = 45 <= direction <= 135
    if is_vertical:
        min_y, max_y = np.min(points[:, 1]), np.max(points[:, 1])
        min_x, max_x = model_robust.predict_x([min_y, max_y])
    else:
        min_x, max_x = np.min(points[:, 0]), np.max(points[:, 0])
        min_y, max_y = model_robust.predict_y([min_x, max_x])
    return LineSegment2D([(min_x, min_y), (max_x, max_y)]), inliers


def _fit_line_string_rec(points: np.ndarray, *args, min_line_length=40, **kwargs) -> np.ndarray:
    line = fit_ransac_line(points, *args, **kwargs)[0]
    if len(points) <= min_line_length:
        return np.array(line.coords)

    projections = ((point, line.project(Point(point))) for point in points)
    projections = list(sorted(projections, key=itemgetter(1)))
    points = np.array([p for p, _ in projections])
    m = points.shape[0] // 2
    coords_1, coords_2 = (_fit_line_string_rec(points[:m], *args, min_line_length=min_line_length, **kwargs),
                          _fit_line_string_rec(points[m:], *args, min_line_length=min_line_length, **kwargs))
    return np.vstack([coords_1[:-1], [(coords_1[-1] + coords_2[0]) / 2], coords_2[1:]])


def fit_line_string(points: np.ndarray, min_samples=2, stop_sample_num=500, residual_threshold=10) -> LineString:
    """
    Fit polyline to points, by splitting points along line in two halves and fitting polyline recursively
    :param points: numpy array of N 2D points of shape (N, 2)
    :param min_samples: the minimum number of data points to fit a model to.
    :param stop_sample_num: stop iteration if at least this number of inliers are found.
    :param residual_threshold: maximum distance for a data point to be classified as an inlier.
    :return: polyline fitted to points
    """
    return LineString(_fit_line_string_rec(points, min_samples, stop_sample_num, residual_threshold))
