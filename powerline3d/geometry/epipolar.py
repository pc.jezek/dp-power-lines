from __future__ import annotations

import cv2
import numpy as np


def get_projection_matrix(K: np.ndarray, R: np.ndarray, t: np.ndarray) -> np.ndarray:
    return K @ np.hstack([R, t.reshape(3, 1)])


def get_cross_product_mtx(x: np.ndarray) -> np.ndarray:
    """
    :return: matrix that realizes the cross product with x
    """
    x1, x2, x3 = x
    return np.array([[0, -x3, x2],
                     [x3, 0, -x1],
                     [-x2, x1, 0]])


def get_essential_matrix(R: np.ndarray, t: np.ndarray) -> np.ndarray:
    t_cross = get_cross_product_mtx(t)
    return t_cross @ R


def get_fundamental_matrix(K: np.ndarray, R: np.ndarray, t: np.ndarray, normalize=True) -> np.ndarray:
    E = get_essential_matrix(R, t)
    F = np.linalg.inv(K).T @ E @ np.linalg.inv(K)
    return F / F[2, 2] if normalize else F


def reconstruct_3d_points(P1, P2, points1, points2) -> np.ndarray:
    """Reconstruct 3D point by least squares triangulation"""
    p3D = cv2.triangulatePoints(P1, P2, points1.T, points2.T).astype(float)
    p3D /= p3D[3]
    return p3D[:3].T
