# %%

from functools import reduce

import cv2
import numpy as np
import scipy.stats as st


def rotate_image(image: np.ndarray, angle: int, flags=cv2.INTER_LINEAR) -> np.ndarray:
    """Rotate image by angle in degrees"""
    c_y, c_x = np.array(image.shape) / 2
    rot_mat = cv2.getRotationMatrix2D((round(c_x), round(c_y)), angle, 1)
    h, w = image.shape
    result = cv2.warpAffine(image, rot_mat, (w, h), flags=flags)
    return result


def get_modified_prewitt(k_size: int, angle: int, direction=1, multiply_with_gauss=False) -> np.ndarray:
    """
    Return rotated Prewitt filter kernel for edge detection
    :param k_size: kernel size
    :param angle: rotate kernel by this angle in degrees
    :param direction: left-to-right or right-to-left edges
    :param multiply_with_gauss: optionally multiply with 1D gaussian to emphasize the pixels close to the center
    :return: k_size x k_size rotated Prewitt filter kernel
    """
    assert k_size % 2 == 1
    k_larger = (k_size * 2 - 1)
    k_offset = (k_larger - k_size) // 2
    l_y = k_larger // 2
    prewitt_kernel = np.zeros((k_larger, k_larger))
    cv2.line(prewitt_kernel, (0, l_y - 1), (k_larger - 1, l_y - 1), 1 * direction, 1)
    cv2.line(prewitt_kernel, (0, l_y), (k_larger - 1, l_y), 0, 1)
    cv2.line(prewitt_kernel, (0, l_y + 1), (k_larger - 1, l_y + 1), -1 * direction, 1)
    if multiply_with_gauss:
        gauss_1d = np.linspace(st.norm.ppf(0.01), st.norm.ppf(0.99), k_larger)
        gauss_1d = st.norm.pdf(gauss_1d)
        gauss_1d = np.interp(gauss_1d, (gauss_1d.min(), gauss_1d.max()), (0, +2))
        prewitt_kernel[l_y - 1, :] *= gauss_1d
        prewitt_kernel[l_y + 1, :] *= gauss_1d
    prewitt_kernel = rotate_image(prewitt_kernel, angle)
    return prewitt_kernel[k_offset:-k_offset, k_offset:-k_offset]


def decorrstretch(A: np.ndarray, tol=None) -> np.ndarray:
    """
    Taken from: https://github.com/Dan-in-CA/decorrstretch/blob/master/ds.py
    No license information available
    """
    orig_shape = A.shape
    A = A.reshape((-1, 3)).astype(np.float)
    cov = np.cov(A.T)
    sigma = np.diag(np.sqrt(cov.diagonal()))
    eigval, V = np.linalg.eig(cov)
    S = np.diag(1 / np.sqrt(eigval))
    mean = np.mean(A, axis=0)
    A -= mean
    T = reduce(np.dot, [sigma, V, S, V.T])
    offset = mean - np.dot(mean, T)
    A = np.dot(A, T)
    A += mean + offset
    B = A.reshape(orig_shape)
    for b in range(3):
        if tol:
            low, high = np.percentile(B[:, :, b], 100 * tol), np.percentile(B[:, :, b], 100 - 100 * tol)
            B[B < low] = low
            B[B > high] = high
        B[:, :, b] = 255 * (B[:, :, b] - B[:, :, b].min()) / (B[:, :, b].max() - B[:, :, b].min())
    return B.astype(np.uint8)


def segment_power_lines_prewitt(img: np.ndarray, kernel_size=31, angle=90) -> np.ndarray:
    """
    Segment power lines using:
    1. decorrelation stretch
    2. 5x5 blur
    3. Filtering with Prewitt kernel of size kernel_size, rotated by angle
    4. morphological operations to reduce noise
    :param img: input image
    :param kernel_size: Prewitt kernel size
    :param angle: rotate Prewitt kernel by this angle in degrees
    :return: segmentation mask (single-channel image in {0, ..., 255})
    """
    img_decorrelated = decorrstretch(img)

    decorrelated = img_decorrelated[:, :, 0].astype(float)
    blur = cv2.blur(decorrelated, (5, 5))
    kernel = get_modified_prewitt(kernel_size, angle)
    prewitt = cv2.filter2D(blur, -1, kernel)

    q01, q99 = np.quantile(prewitt, [0.01, 0.99])
    _, thresholded = cv2.threshold(prewitt, q99, 255, cv2.THRESH_BINARY)

    kernel_morph = np.ones((3, 3))
    filtered = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, np.round(kernel_morph).astype(np.uint8), iterations=1)
    filtered = cv2.morphologyEx(filtered,
                                cv2.MORPH_OPEN,
                                np.round(kernel_morph).astype(np.uint8),
                                iterations=1,
                                borderValue=0)

    return filtered.astype(np.uint8)
