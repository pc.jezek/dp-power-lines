from __future__ import annotations

import numpy as np
from mmseg.apis import inference_segmentor


def detect_lines(model, img: np.ndarray) -> np.ndarray:
    """Use neural network to segment lines from images"""
    res = inference_segmentor(model, img)[0]
    return np.stack((res,) * 3, axis=-1)


def segment_power_lines_neuralnet(model,
                                  img: np.ndarray,
                                  win_size_w=1920,
                                  win_size_h=1080,
                                  crop_edges=20) -> np.ndarray:
    """
    Segment power lines in images using a trained neural network.
    1. crop the initial image into smaller segments of win_size_h x win_size_w pixels
    2. run inference on each segment
    3. compose the results back into a segmentation image
    :param model: trained neural network with loaded weights
    :param img: input image
    :param win_size_w: width of the cropping window
    :param win_size_h: height of the cropping window
    :param crop_edges: thickness of edges to crop from the neural network output (overlap)
    :return: segmentation mask (single-channel image in {0, ..., 255})
    """
    height, width = img.shape[:2]

    mask = np.zeros_like(img).astype(float)
    for r in range(0, img.shape[0], win_size_h - 2 * crop_edges):
        for c in range(0, img.shape[1], win_size_w - 2 * crop_edges):
            r_end = min(r + win_size_h, height)
            c_end = min(c + win_size_w, width)

            crop_r = crop_edges
            crop_c = crop_edges
            crop_r_end = crop_edges
            crop_c_end = crop_edges

            res = detect_lines(model, img[r:r_end, c:c_end])

            mask[r + crop_r:r_end - crop_r_end,
            c + crop_c:c_end - crop_c_end] = res[crop_r:(-crop_r_end or None), crop_c:(-crop_c_end or None)]
    return np.uint8(mask)[:, :, 0] * 255
