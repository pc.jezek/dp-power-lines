import functools
import glob
import operator
import os

IMAGE_FILE_TYPES = ('tiff', 'tif', 'jpeg', 'png', 'TIFF', 'TIF', 'JPEG', 'PNG')


def load_image_paths(folder_path: str) -> list[str]:
    """Glob all supported images in a given folder"""
    image_paths = [glob.glob(os.path.join(folder_path, f'*.{ext}'))
                   for ext
                   in IMAGE_FILE_TYPES]
    image_paths = functools.reduce(operator.iconcat, image_paths, [])
    if not len(image_paths):
        raise FileNotFoundError('Empty input directory')

    return image_paths
