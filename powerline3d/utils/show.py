import cv2
import numpy as np

WAIT_KEY_DELAY = 1


def imshow(image: np.ndarray, winname='noot noot', delay=WAIT_KEY_DELAY):
    """
    Shows an image in a separate cv2 window.

    Exit by pressing 'q' or closing the window

    The function waits for a key event infinitely delay == 0
    or for delay milliseconds, when it is positive

    :param winname: window name
    :param image: numpy array (w x h x 3)
    :param delay: how long should the function wait for a key event (0 means wait infinitely)
    """
    cv2.namedWindow(winname)

    while cv2.getWindowProperty(winname, cv2.WND_PROP_VISIBLE):
        cv2.imshow(winname, image)
        key = cv2.waitKey(delay) & 0xFF
        if key == ord('q'):
            break
    cv2.destroyWindow(winname)


def wait_key(winname='noot noot', delay=WAIT_KEY_DELAY):
    while cv2.getWindowProperty(winname, cv2.WND_PROP_VISIBLE):
        key = cv2.waitKey(delay) & 0xFF
        if key == ord('q'):
            break
    cv2.destroyWindow(winname)
