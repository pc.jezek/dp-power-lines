"""

Contains modified parts of code from OpenCV tutorial (https://docs.opencv.org/4.x/dc/dbb/tutorial_py_calibration.html)
which is licenced under Apache license:

   Copyright 2021 , OpenCV team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
Full OpenCV licence: https://github.com/opencv/opencv/blob/master/LICENSE
"""

from __future__ import annotations
from typing import Tuple, Iterable

import progressbar

import powerline3d.utils.show as cvshow
import numpy as np
import cv2
import json


def get_norm_matrix(image_width: int, image_height: int) -> np.ndarray:
    """Matrix transforming normalized image coordinates to sensor coordinates"""
    H = np.eye(3)
    H[0, 0] = H[1, 1] = max(image_width, image_height)
    H[0, 2] = (image_width - 1) / 2
    H[1, 2] = (image_height - 1) / 2
    return H


def get_inv_norm_matrix(image_width: int, image_height: int) -> np.ndarray:
    """Matrix transforming sensor coordinates to normalized image coordinates"""
    H = np.eye(3)
    H[2, 2] = max(image_width, image_height)
    H[0, 2] = - (image_width - 1) / 2
    H[1, 2] = - (image_height - 1) / 2
    return H


def transform_point(transfom_matrix, point: [float]):
    point_transformed = transfom_matrix @ np.array([[point[0], point[1], 1]]).T
    point_transformed = point_transformed.reshape(3)
    point_transformed /= point_transformed[2]
    return point_transformed[:2]


def to_normalized_image_coordinates(point: [float], image_width: int, image_height: int):
    """Transform point in sensor coordinates to normalized image coordinates"""
    return transform_point(get_inv_norm_matrix(image_width, image_height), point)


def from_normalized_image_coordinates(point: [float], image_width: int, image_height: int):
    """Transform point in normalized image coordinates to sensor coordinates"""
    return transform_point(get_norm_matrix(image_width, image_height), point)


def to_opensfm_calibration(image_shape: tuple[int, int], mtx: np.ndarray, dist: np.ndarray) -> dict[str, any]:
    """From OpenCV intrinsics (K, dist) representation to OpenSFM representation"""
    h, w = image_shape

    # Normalization matrix for openSFM: (https://www.opensfm.org/docs/geometry.html#camera-models)
    cx, cy = [mtx[0, 2], mtx[1, 2]]
    fx, fy = [mtx[0, 0], mtx[1, 1]]
    cx, cy = to_normalized_image_coordinates([cx, cy], w, h)
    fx, fy = np.array([fx, fy]) / max(w, h)

    k1, k2, p1, p2, k3 = dist[0]
    return {
        'projection_type': 'brown',
        'width': w,
        'height': h,
        'focal_x': fx,
        'focal_y': fy,
        'c_x': cx,
        'c_y': cy,
        'k1': k1, 'k2': k2, 'p1': p1, 'p2': p2, 'k3': k3
    }


def load_opensfm_calibration(camera: dict) -> tuple[np.ndarray, np.ndarray]:
    """From OpenSFM intrinsics (K, dist) representation to OpenCV representation"""
    projection_type = camera['projection_type']
    h, w = camera['height'], camera['width']

    if projection_type == 'perspective':
        focal_x = focal_y = camera['focal']
        c_x = c_y = 0
        dist = np.array([[camera['k1'], camera['k2'], 0, 0, 0]])
    else:
        focal_x, focal_y = camera['focal_x'], camera['focal_y']
        c_x, c_y = camera['c_x'], camera['c_y']
        dist = np.array([[camera['k1'], camera['k2'], camera['p1'], camera['p2'], camera['k3']]])

    mtx = np.zeros((3, 3))
    mtx[0, 2], mtx[1, 2] = from_normalized_image_coordinates([c_x, c_y], w, h)
    mtx[0, 0], mtx[1, 1] = focal_x * max(w, h), focal_y * max(w, h)
    mtx[2, 2] = 1
    return mtx, dist


def calibrate_camera(checkerboard_shape: Tuple[int, int],
                     images: Iterable[np.ndarray],
                     interactive=True, resize=1, log_to_console=False):
    """
    Obtain camera intrinsics from images of a checkerboard pattern using Zhang's method
    Code from official documentation (https://docs.opencv.org/4.x/dc/dbb/tutorial_py_calibration.html),
    modified for high-resolution images
    :param checkerboard_shape: shape of the checkerboard pattern
    :param images: images capturing the full checkerboard from several angles (should be at least 20)
    :param interactive: show detected corners
    :param resize: fraction (0, 1] of the image size for resizing - recommended for larger resolution
    :param log_to_console: show progress in console
    :return: output of openCV calibrateCamera
    """
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 50, 0.001)
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((checkerboard_shape[0] * checkerboard_shape[1], 3), np.float32)
    objp[:, :2] = np.mgrid[0:checkerboard_shape[0], 0:checkerboard_shape[1]].T.reshape(-1, 2)
    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d point in real world space
    imgpoints = []  # 2d points in image plane.
    success_cnt = 0
    images = progressbar.progressbar(images) if log_to_console else images
    for img in images:
        img_small = resize_img(img, resize) if resize else img
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray_small = cv2.cvtColor(img_small, cv2.COLOR_BGR2GRAY)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray_small, checkerboard_shape, None)
        # If found, add object points, image points (after refining them)
        img = np.copy(img)
        if ret == True:
            success_cnt += 1
            objpoints.append(objp)

            corners *= 1 / resize
            corners2 = cv2.cornerSubPix(gray, corners, (51, 51), (-1, -1), criteria)
            imgpoints.append(corners2)
            # Draw and display the corners
            cv2.drawChessboardCorners(img, checkerboard_shape, corners2, ret)

        if interactive:
            cvshow.imshow(img)

    cv2.destroyAllWindows()

    res = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

    _, mtx, dist, rvecs, tvecs = res

    mean_error = 0
    for i in range(len(objpoints)):
        imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
        error = cv2.norm(imgpoints[i], imgpoints2, cv2.NORM_L2) / len(imgpoints2)
        mean_error += error
    mean_error /= len(objpoints)

    if log_to_console:
        print(f"Successfully detected checkerboard in {success_cnt} images")
        print("total error: {}".format(mean_error))

    return res


def resize_img(img: np.ndarray, scale: float) -> np.ndarray:
    return cv2.resize(img, None, fx=scale, fy=scale)
