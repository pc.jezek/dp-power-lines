import colorsys
from itertools import cycle
from typing import Union, Tuple

import cv2
import numpy as np
import shapely.geometry as sg

from powerline3d.geometry.planar import LineSegment2D

rng = np.random.default_rng()

""" Various drawing utilities for visualization """


def draw_geometric_line(dst, line: LineSegment2D, color: Union[int, Tuple[int, int, int]] = 255, *args, **kwargs):
    getp = lambda x: np.round(x.coords[0]).astype(int)
    return cv2.line(dst, getp(line.p1), getp(line.p2), color, *args, **kwargs)


Color = tuple[int, int, int]


def hsv2rgb(h: float, s: float, v: float) -> Color:
    r, g, b = colorsys.hsv_to_rgb(h, s, v)
    return round(r * 255), round(g * 255), round(b * 255)


def get_uniform_cmap(n_colors: int, shuffle=True) -> list[Color]:
    cmap = np.linspace(0, 1, n_colors + 2)[:-2]
    if shuffle:
        rng.shuffle(cmap)
    return [hsv2rgb(h, 1, 1) for h in cmap]


def _get_colors(n_colors: int, colors=None, rand_color=False) -> list[Color]:
    if rand_color:
        colors = get_uniform_cmap(n_colors, True)
    elif colors is None:
        colors = cycle([(255, 0, 0)])
    else:
        assert len(colors) == n_colors
        colors = iter(colors)
    return colors


def draw_lines(dst: np.ndarray, lines: [LineSegment2D], colors=None, rand_color=False, thickness=1) -> np.ndarray:
    colors = _get_colors(len(lines), colors, rand_color)
    if lines is not None:
        for line, color in zip(lines, colors):
            draw_geometric_line(dst, line, color, thickness, cv2.LINE_4)
    return dst


def draw_linestring(dst: np.ndarray, line_string: sg.LineString, color=255, *args, **kwargs) -> np.ndarray:
    prev_p = line_string.coords[0]
    for next_p in line_string.coords[1:]:
        l = LineSegment2D([prev_p, next_p])
        draw_geometric_line(dst, l, color, *args, **kwargs)
        prev_p = next_p
    return dst


def draw_points(img: np.ndarray, points, colors=None, rand_color=False, radius=5, thickness=20) -> np.ndarray:
    colors = _get_colors(len(points), colors, rand_color)
    for p, color in zip(points, colors):
        img = cv2.circle(img, tuple(np.array(p.coords[0], int)), radius, color, thickness)
    return img


def visualize_mask(mask: np.ndarray, img, bg_opacity=0.3) -> np.ndarray:
    background = (cv2.cvtColor(img, cv2.COLOR_BGR2RGB) * bg_opacity).astype(np.uint8)
    return cv2.add(mask, background)
