import argparse
import json
import os
import os.path as osp
import pickle
import sys

import numpy as np
import yaml
from PIL import Image
from mmcv.parallel import MMDataParallel
from mmcv.runner import load_checkpoint
from mmseg.apis import single_gpu_test
from mmseg.datasets import build_dataset, build_dataloader
from mmseg.datasets.builder import DATASETS
from mmseg.datasets.custom import CustomDataset
from mmseg.models import build_segmentor

"""
Test neural network using test dataset
Input: 
    - trained weights
    - test dataset
Outpus:
    - test score by various metrics
"""

rng = np.random.default_rng(seed=42)

arg_parser = argparse.ArgumentParser()

arg_parser.add_argument('--config',
                        type=str,
                        help="power line reconstruction configuration",
                        default="config/power_lines_cfg.yaml")

args = arg_parser.parse_args()
IMAGE_DIR = 'JPEGImages'
SEG_IMAGE_DIR = 'SegmentationClassPNG'
CLASSES_FILE = 'class_names.txt'


def init_test_model(cfg, checkpoint, device='cuda:0'):
    img = Image.open(osp.join(cfg.data_root, SEG_IMAGE_DIR, '07_1875.png'))
    with open(osp.join(cfg.data_root, CLASSES_FILE)) as f:
        classes = f.read().splitlines()
        palette = np.array(img.getpalette()).reshape((-1, 3))[:len(classes)].tolist()

    @DATASETS.register_module(force=True)
    class TtplaDataset(CustomDataset):
        CLASSES = classes
        PALETTE = palette

        def __init__(self, split, **kwargs):
            super().__init__(img_suffix='.jpg', seg_map_suffix='.png',
                             split=split, **kwargs)
            assert osp.exists(self.img_dir) and self.split is not None

    cfg.model.pretrained = None
    cfg.model.train_cfg = None
    model = build_segmentor(cfg.model, test_cfg=config.get('test_cfg'))
    checkpoint = load_checkpoint(model, checkpoint, map_location='cpu')
    model.CLASSES = checkpoint['meta']['CLASSES']
    model.PALETTE = checkpoint['meta']['PALETTE']
    model.to(device)
    return model


if __name__ == '__main__':
    with open(args.config) as f:
        config = yaml.safe_load(f)

    if not os.path.exists(config['segmentation']['nn_train']['ttpla_dataset']):
        print("Error: TTPLA dataset missing, please download it using - poe download-ttpla-voc")
        sys.exit(1)

    if not os.path.exists(config['segmentation']['nn_train']['model_work_dir']):
        os.makedirs(config['segmentation']['nn_train']['model_work_dir'])

    with open(config['segmentation']['nn_inference']['model_config'], 'rb') as f:
        cfg = pickle.load(f)

    model = init_test_model(cfg, config['segmentation']['nn_inference']['model_weights'])

    dataset = build_dataset(cfg.data.test)
    data_loader = build_dataloader(dataset,
                                   samples_per_gpu=1,
                                   workers_per_gpu=cfg.data.workers_per_gpu,
                                   dist=False,
                                   shuffle=False)

    model.CLASSES = dataset.CLASSES
    model = MMDataParallel(model, device_ids=[0])
    results = single_gpu_test(model, data_loader, pre_eval=True)
    metrics = dataset.evaluate(results, pre_eval=True)
    with open(os.path.join(config['segmentation']['nn_train']['model_work_dir'], 'test_metrics.json'), 'w') as f:
        json.dump(metrics, f)
