from __future__ import annotations

import argparse
import glob
import json
import os
import sys

import cv2
import numpy as np
import progressbar
import yaml
from PIL import Image

import powerline3d.utils.draw as cvdraw
from powerline3d.detection import detect_power_lines
from powerline3d.utils.io import load_image_paths

"""
Run Power line detection pipeline.
Input: binary segmentation masks for each image
Output: list of detected power lines for each image
"""

arg_parser = argparse.ArgumentParser()

arg_parser.add_argument('num_power_lines',
                        type=int,
                        help="number of power lines visible in input data")

arg_parser.add_argument('--config',
                        type=str,
                        help="power line reconstruction configuration",
                        default="config/power_lines_cfg.yaml")

arg_parser.add_argument('--noviz', action='store_true', help="do not create visualisation images")

args = arg_parser.parse_args()

if __name__ == '__main__':
    with open(args.config) as f:
        cfg = yaml.safe_load(f)

    images_paths = load_image_paths(cfg['segmentation']['output'])

    if os.path.exists(cfg['detection']['output']):
        print("Output directory already exists:", cfg['detection']['output'])
        sys.exit(1)
    os.makedirs(cfg['detection']['output'])


    def visualize_power_lines(power_lines, image):
        dst = np.zeros_like(image)
        cmap = cvdraw.get_uniform_cmap(len(power_lines))
        for cluster, color in zip(power_lines, cmap):
            dst = cvdraw.draw_linestring(dst, cluster, color, 2, lineType=cv2.LINE_AA)
        return cvdraw.visualize_mask(dst, image, bg_opacity=0.5)


    for image_path in progressbar.progressbar(images_paths):
        mask = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
        basename = os.path.basename(image_path).rsplit('.')[0]
        power_lines = detect_power_lines(mask,
                                         args.num_power_lines,
                                         cluster_angle_thr=cfg['detection']['cluster_angle_thr'],
                                         cluster_dist_thr=cfg['detection']['cluster_dist_thr'],
                                         v_step=cfg['detection']['v_step'],
                                         max_line_gap_steps=cfg['detection']['max_line_gap_steps'],
                                         v_step_overlap=cfg['detection']['v_step_overlap'],
                                         min_v_steps_filter=cfg['detection']['min_v_steps_filter'])
        with open(os.path.join(cfg['detection']['output'], basename + '.json'), 'w') as f:
            power_lines_serializable = [list(power_line.coords) for power_line in power_lines]
            json.dump(power_lines_serializable, f)
        if not args.noviz:
            orig_file_name = glob.glob(f"{os.path.join(cfg['undistorted_image_folder'], basename)}.*")
            if not orig_file_name:
                print(f"Input image {orig_file_name} not found in {cfg['undistorted_image_folder']}")
                sys.exit(2)
            orig_file_name = orig_file_name[0]
            if not os.path.exists(orig_file_name):
                print("Original image file does not exist", orig_file_name)
                sys.exit(1)

            img = cv2.imread(orig_file_name)
            visualisation = visualize_power_lines(power_lines, img)
            visualisation = cv2.resize(visualisation, None, fx=1 / 4, fy=1 / 4)
            img = Image.fromarray(visualisation)
            img.save(os.path.join(cfg['detection']['output'], basename + '.png'))
