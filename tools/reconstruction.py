from __future__ import annotations

import argparse

import yaml

from powerline3d.reconstruction import reconstruct_power_lines
from powerline3d.reconstruction.utils import load_shots, write_ply_point_cloud, load_geo_ref

"""
Run Power line reconstruction pipeline.
Input: 
    - opensfm reconstruction (intrinsics, extrinsics)
    - detected power lines for each image
Output: 3D point cloud of power lines
"""

arg_parser = argparse.ArgumentParser()

arg_parser.add_argument('num_power_lines',
                        type=int,
                        help="number of power lines visible in input data")

arg_parser.add_argument('--config',
                        type=str,
                        help="power line reconstruction configuration",
                        default="config/power_lines_cfg.yaml")
arg_parser.add_argument('--nocatenary',
                        action='store_true',
                        help="disable catenary curve fitting")

args = arg_parser.parse_args()

if __name__ == '__main__':
    with open(args.config) as f:
        cfg = yaml.safe_load(f)

    shots = load_shots(cfg['odm']['reconstruction_path'], cfg['detection']['output'], cfg['undistorted_image_folder'])
    shots_filtered = {k: v for k, v in shots.items() if v.power_lines}
    power_lines_3d = reconstruct_power_lines(shots_filtered.values(), args.num_power_lines, args.nocatenary)

    geo_ref = load_geo_ref(cfg['odm']['geo_ref_file']) if cfg['odm']['geo_ref_file'] else None
    write_ply_point_cloud(cfg['reconstruction']['point_cloud_output'], power_lines_3d, geo_ref)

    print(f'Success. Point cloud written in {cfg["reconstruction"]["point_cloud_output"]}')
    if geo_ref is None:
        print('No georeferencing provided')
    else:
        print(f'Coordinates are in {geo_ref["format"]}')
