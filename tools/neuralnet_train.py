"""
Based on MMSegmentation tutorial
(https://github.com/open-mmlab/mmsegmentation/blob/master/demo/MMSegmentation_Tutorial.ipynb),
which is licenced under Apache license:

   Copyright 2020 The MMSegmentation Authors.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
import argparse
import os
import os.path as osp
import pickle
import sys

import mmcv
import numpy as np
import yaml
from PIL import Image
from mmcv import Config
from mmseg.apis import set_random_seed
from mmseg.apis import train_segmentor
from mmseg.datasets import build_dataset
from mmseg.datasets.builder import DATASETS
from mmseg.datasets.custom import CustomDataset
from mmseg.models import build_segmentor

"""
Train the Deeplabv3+ neural network using MMSegmentation
Input: training, validation dataset
Output: trained neuralnet weights
"""

# 420 is ten times better than 42 - Elon Musk
rng = np.random.default_rng(seed=420)

arg_parser = argparse.ArgumentParser()

arg_parser.add_argument('--config',
                        type=str,
                        help="power line reconstruction configuration",
                        default="config/power_lines_cfg.yaml")

args = arg_parser.parse_args()

IMAGE_DIR = 'JPEGImages'
SEG_IMAGE_DIR = 'SegmentationClassPNG'
CLASSES_FILE = 'class_names.txt'
SPLIT_DIR = 'splits'


def split_dataset(data_root):
    mmcv.mkdir_or_exist(osp.join(data_root, SPLIT_DIR))
    filename_list = [osp.splitext(filename)[0] for filename in mmcv.scandir(
        osp.join(data_root, IMAGE_DIR), suffix='.jpg')]
    rng.shuffle(filename_list)

    train_length = int(len(filename_list) * 10 / 12)
    val_length = int(len(filename_list) * 1 / 12)

    train_end = train_length
    val_end = train_end + val_length

    with open(osp.join(data_root, SPLIT_DIR, 'train.txt'), 'w') as f:
        # select first 4/5 as train set

        f.writelines(line + '\n' for line in filename_list[:train_end])
    with open(osp.join(data_root, SPLIT_DIR, 'val.txt'), 'w') as f:
        # select last 1/5 as train set
        f.writelines(line + '\n' for line in filename_list[train_end:val_end])
    with open(osp.join(data_root, SPLIT_DIR, 'test.txt'), 'w') as f:
        # select last 1/5 as train set
        f.writelines(line + '\n' for line in filename_list[val_end:])


def build_config(data_root, config_base, checkpoint_file, work_dir):
    img = Image.open(osp.join(data_root, SEG_IMAGE_DIR, '07_1875.png'))
    with open(osp.join(data_root, CLASSES_FILE)) as f:
        classes = f.read().splitlines()
        palette = np.array(img.getpalette()).reshape((-1, 3))[:len(classes)].tolist()

    @DATASETS.register_module(force=True)
    class TtplaDataset(CustomDataset):
        CLASSES = classes
        PALETTE = palette

        def __init__(self, split, **kwargs):
            super().__init__(img_suffix='.jpg', seg_map_suffix='.png',
                             split=split, **kwargs)
            assert osp.exists(self.img_dir) and self.split is not None

    cfg = Config.fromfile(config_base)

    bg_weight = 0.05
    pl_weight = 1 - bg_weight
    # Since we use ony one GPU, BN is used instead of SyncBN
    cfg.norm_cfg = dict(type='BN', requires_grad=True)
    cfg.model.backbone.norm_cfg = cfg.norm_cfg
    cfg.model.decode_head.norm_cfg = cfg.norm_cfg
    cfg.model.decode_head.loss_decode = dict(
        type='CrossEntropyLoss',
        use_sigmoid=False,
        loss_weight=1.0,
        class_weight=[bg_weight, pl_weight])
    cfg.model.auxiliary_head.norm_cfg = cfg.norm_cfg
    # modify num classes of the model in decode/auxiliary head
    cfg.model.decode_head.num_classes = len(classes)
    cfg.model.auxiliary_head.num_classes = len(classes)

    # Modify dataset type and path
    cfg.dataset_type = 'TtplaDataset'
    cfg.data_root = data_root

    cfg.data.samples_per_gpu = 8
    cfg.data.workers_per_gpu = 8

    cfg.img_norm_cfg = dict(
        mean=[123.675, 116.28, 103.53], std=[58.395, 57.12, 57.375], to_rgb=True)
    cfg.crop_size = (256, 256)
    cfg.train_pipeline = [
        dict(type='LoadImageFromFile'),
        dict(type='LoadAnnotations'),
        dict(type='Resize', img_scale=(1920, 1080), ratio_range=(0.5, 2.0)),
        dict(type='RandomCrop', crop_size=cfg.crop_size, cat_max_ratio=0.75),
        dict(type='RandomFlip', flip_ratio=0.5),
        dict(type='PhotoMetricDistortion'),
        dict(type='Normalize', **cfg.img_norm_cfg),
        dict(type='Pad', size=cfg.crop_size, pad_val=0, seg_pad_val=255),
        dict(type='DefaultFormatBundle'),
        dict(type='Collect', keys=['img', 'gt_semantic_seg']),
    ]

    cfg.test_pipeline = [
        dict(type='LoadImageFromFile'),
        dict(
            type='MultiScaleFlipAug',
            img_scale=(1920, 1080),
            # img_ratios=[0.5, 0.75, 1.0, 1.25, 1.5, 1.75],
            flip=False,
            transforms=[
                dict(type='Resize', keep_ratio=True),
                dict(type='RandomFlip'),
                dict(type='Normalize', **cfg.img_norm_cfg),
                dict(type='ImageToTensor', keys=['img']),
                dict(type='Collect', keys=['img']),
            ])
    ]

    cfg.data.train.type = cfg.dataset_type
    cfg.data.train.data_root = cfg.data_root
    cfg.data.train.img_dir = IMAGE_DIR
    cfg.data.train.ann_dir = SEG_IMAGE_DIR
    cfg.data.train.pipeline = cfg.train_pipeline
    cfg.data.train.split = osp.join(SPLIT_DIR, 'train.txt')

    cfg.data.val.type = cfg.dataset_type
    cfg.data.val.data_root = cfg.data_root
    cfg.data.val.img_dir = IMAGE_DIR
    cfg.data.val.ann_dir = SEG_IMAGE_DIR
    cfg.data.val.pipeline = cfg.test_pipeline
    cfg.data.val.split = osp.join(SPLIT_DIR, 'val.txt')

    cfg.data.test.type = cfg.dataset_type
    cfg.data.test.data_root = cfg.data_root
    cfg.data.test.img_dir = IMAGE_DIR
    cfg.data.test.ann_dir = SEG_IMAGE_DIR
    cfg.data.test.pipeline = cfg.test_pipeline
    cfg.data.test.split = osp.join(SPLIT_DIR, 'test.txt')

    # We can still use the pre-trained Mask RCNN model though we do not need to
    # use the mask branch
    cfg.load_from = checkpoint_file

    # Set up working dir to save files and logs.
    cfg.work_dir = osp.join(work_dir, 'model')

    cfg.runner.max_iters = 50000
    cfg.log_config.interval = 100
    cfg.evaluation.interval = 1000
    cfg.checkpoint_config.interval = 1000
    cfg.checkpoint_config.meta = dict(CLASSES=classes, PALETTE=palette)

    cfg.seed = 0
    set_random_seed(0, deterministic=False)
    cfg.gpu_ids = range(1)
    return cfg


if __name__ == '__main__':
    with open(args.config) as f:
        config = yaml.safe_load(f)

    if not os.path.exists(config['segmentation']['nn_train']['ttpla_dataset']):
        print("Error: TTPLA dataset missing, please download it using - poe download-ttpla-voc")
        sys.exit(1)

    if not os.path.exists(config['segmentation']['nn_train']['model_checkpoint']):
        print("Error: Model checkpoint missing, please download it using - poe download-checkpoint")
        sys.exit(1)

    if os.path.exists(config['segmentation']['nn_train']['model_work_dir']):
        print("Error: Output directory already exists:", config['segmentation']['nn_train']['model_work_dir'])
        sys.exit(1)

    os.makedirs(config['segmentation']['nn_train']['model_work_dir'])

    split_dataset(config['segmentation']['nn_train']['ttpla_dataset'])
    cfg = build_config(config['segmentation']['nn_train']['ttpla_dataset'],
                       config['segmentation']['nn_train']['model_config_base'],
                       config['segmentation']['nn_train']['model_checkpoint'],
                       config['segmentation']['nn_train']['model_work_dir'])

    print(f'Config:\n{cfg.pretty_text}')

    with open(config['segmentation']['nn_inference']['model_config'], 'wb') as f:
        pickle.dump(cfg, f)

    datasets = [build_dataset(cfg.data.train)]

    # Build the detector
    model = build_segmentor(cfg.model, train_cfg=cfg.get('train_cfg'), test_cfg=cfg.get('test_cfg'))
    # Add an attribute for visualization convenience
    model.CLASSES = datasets[0].CLASSES

    # Create work_dir
    train_segmentor(model, datasets, cfg, distributed=False, validate=True, meta=dict())
