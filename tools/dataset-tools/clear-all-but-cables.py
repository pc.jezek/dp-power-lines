"""
Based on TTPLA script
(https://github.com/R3ab/ttpla_dataset/blob/master/scripts/remove_void.py),
which is licenced under Apache license:

   Copyright 2020 The MMSegmentation Authors.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

"""

import argparse
import json
import os
import re
import shutil

from progressbar import progressbar

"""
Clear all labels except power lines from the TTPLA dataset
"""

ap = argparse.ArgumentParser()
ap.add_argument('input_img_dir', help='path to imgs with annotations')
ap.add_argument('output_dir', help='output directory')
args = ap.parse_args()

jsons_names = [img for img in os.listdir(args.input_img_dir) if img.endswith(".json")]

all_labels = {}

for p in [args.output_dir]:
    if not os.path.exists(p):
        os.makedirs(p)

nskipped = 0

for js in progressbar(jsons_names):
    fpath = os.path.join(args.input_img_dir, js)
    fout = os.path.join(args.output_dir, js)
    fdata = json.load(open(fpath, 'r'))
    newdata = {}
    ncables = 0
    for key in fdata:
        if key == 'shapes':
            newdata['shapes'] = []
            for m in fdata['shapes']:
                m['label'] = m['label'].lower()
                if m['label'] != 'cable':
                    continue
                ncables += 1
                newdata['shapes'].append(m)
        else:
            newdata[key] = fdata[key]
    if ncables > 0:
        json.dump(newdata, open(fout, 'w'))
        shutil.copy2(re.sub(r'json$', 'jpg', fpath), args.output_dir)
    else:
        nskipped += 1
print(f'{nskipped} images were skipped, because they contained no wires')
