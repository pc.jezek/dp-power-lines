from __future__ import annotations

import glob
import os

import numpy as np
import json
from argparse import ArgumentParser

import yaml

"""
Plan flight parameters based on camera and lens hardware equipment
Input: hardware specifications, various flight parameters
Output: summary with calculated flight parameters (height, GSD, ...)
"""

DEFAULT_GSD = 2
DEFAULT_FLIGHT_HEIGHT = 70
MAX_MOTION_BLUR = 1.5
DEFAULT_SHUTTER_SPEED = 1 / 500
DEFAULT_APERTURE = 5.6

arg_parser = ArgumentParser(description="UAV flight params calculator")
arg_parser.add_argument('camera', type=str, help="Name of the cameras")
arg_parser.add_argument('--lens', type=str, help="Name of the lenses", default=None)
arg_parser.add_argument('--shutter-speed', type=float, help="Shutter speed in s", default=DEFAULT_SHUTTER_SPEED)
arg_parser.add_argument('--aperture', type=float, help="Aperture size", default=DEFAULT_APERTURE)

group = arg_parser.add_mutually_exclusive_group(required=True)
group.add_argument('--gsd', type=float, help="Ground Sample Distance (in cm)")
group.add_argument('--flight-height', type=float, help="Flight height (in m)")

arg_parser.add_argument('--config',
                        type=str,
                        help="power line reconstruction configuration",
                        default="config/power_lines_cfg.yaml")

args = arg_parser.parse_args()


def print_kv(key, value, key_width=25, end='\n'):
    print(f'{key + ":":<{key_width}}{value}', end=end)


def create_camera_from_file(file):
    try:
        with open(file) as camera_file:
            camera_params = json.load(camera_file)
            focal_length_mm_equiv = None \
                if "focal_length_mm_equiv" not in camera_params \
                else camera_params["focal_length_mm_equiv"]
            return Camera(
                camera_params["name"],
                camera_params["image_width_px"],
                camera_params["image_height_px"],
                camera_params["sensor_width_mm"],
                camera_params["sensor_height_mm"],
                focal_length_mm_equiv=focal_length_mm_equiv
            )
    except KeyError as e:
        print('Missing camera parameter:', e)
        raise
    except FileNotFoundError:
        print_available_cameras(os.path.dirname(file))
        exit(2)


def create_lens_from_file(file):
    try:
        with open(file) as lens_file:
            lens_params = json.load(lens_file)
            return Lens(lens_params["name"], lens_params["focal_length_mm"])
    except KeyError as e:
        print('Missing lens parameter:', e)
        raise
    except FileNotFoundError:
        print_available_lenses(os.path.dirname(file))
        exit(2)


class Camera:
    def __init__(self, name, image_width_px, image_height_px, sensor_width_mm, sensor_height_mm,
                 focal_length_mm_equiv=None):
        self.name = name
        self.im_w = image_width_px
        self.im_h = image_height_px
        self.s_w = sensor_width_mm
        self.s_h = sensor_height_mm
        self.focal_length_mm_equiv = focal_length_mm_equiv

    def print_stats(self):
        print_kv('Camera', self.name)
        print_kv('Image size', f'{self.im_w} x {self.im_h}')
        print_kv('Sensor size', f'{self.s_w}mm x {self.s_h}mm')
        print_kv('Crop factor', f'{self.crop_factor}')
        print_kv('Pixel pitch', f'{self.pixel_pitch:.7f}mm')
        if self.focal_length_mm_equiv:
            print_kv('Builtin lens (35mm eq.)', f'{self.focal_length_mm_equiv:.2f}mm')

    @property
    def pixel_pitch(self):
        return min(self.s_w / self.im_w, self.s_h / self.im_h)

    @property
    def crop_factor(self):
        # Full frame (36mm x 24mm) = 1x crop factor = 36**2 + 24**2 = 1872
        return np.sqrt(1872 / (self.s_w ** 2 + self.s_h ** 2))


class Lens:
    def __init__(self, name, focal_length):
        self.name = name
        self.focal_length = focal_length

    def print_stats(self):
        print_kv('Lens', self.name)
        print_kv('Focal length', f'{self.focal_length}mm')


def get_gsd(camera: Camera, lens: Lens, flight_height_m: float) -> float:
    return ((flight_height_m * camera.pixel_pitch) / lens.focal_length) * 100


def get_flight_height(camera: Camera, lens: Lens, gsd_cm: float) -> float:
    return ((gsd_cm * lens.focal_length) / camera.pixel_pitch) / 100


def get_max_speed(shutter_speed: float, gsd_cm: float, ) -> float:
    return MAX_MOTION_BLUR * (gsd_cm / 100) / shutter_speed


def get_diffraction(aperture: float):
    return 1.22 * 5.5e-7 * aperture * 2 * 1000


def enumerate_jsons(path):
    return [os.path.basename(f) for f in glob.glob(os.path.join(path, f'*.json'))]


def print_available_cameras(path):
    print('The camera was not found, list of available cameras:')
    for camera_file in enumerate_jsons(path):
        print(camera_file.rstrip('.json'))


def print_available_lenses(path):
    print('The lens was not found, list of available lenses:')
    for lens_file in enumerate_jsons(path):
        print(lens_file.rstrip('.json'))


if __name__ == '__main__':
    with open(args.config) as f:
        cfg = yaml.safe_load(f)

    camera_files = enumerate_jsons(cfg['flight_planner']['cameras_folder'])
    lens_files = enumerate_jsons(cfg['flight_planner']['lenses_folder'])

    camera = create_camera_from_file(os.path.join(cfg['flight_planner']['cameras_folder'], args.camera + '.json'))
    if not camera.focal_length_mm_equiv:
        if not args.lens:
            print('Camera has no builtin lens. Please provide the lens specification')
            print('Available lenses are: ')
            for lens in enumerate_jsons(cfg['flight_planner']['lenses_folder']):
                print(lens.rstrip('.json'))
            exit(1)
        lens = create_lens_from_file(os.path.join(cfg['flight_planner']['lenses_folder'], args.lens + '.json'))
    else:
        lens = Lens(camera, camera.focal_length_mm_equiv / camera.crop_factor)

    gsd = args.gsd if args.gsd else DEFAULT_GSD
    flight_height = args.flight_height if args.flight_height else DEFAULT_FLIGHT_HEIGHT

    camera.print_stats()
    if camera.focal_length_mm_equiv:
        equiv_focal_length = camera.focal_length_mm_equiv
    else:
        print('-' * 42)
        lens.print_stats()
        equiv_focal_length = camera.crop_factor * lens.focal_length

    print('-' * 42)
    print_kv('Equiv. focal length', f'{camera.crop_factor * lens.focal_length:.2f}mm')

    if args.flight_height:
        gsd = get_gsd(camera, lens, args.flight_height)
    if args.gsd:
        flight_height = get_flight_height(camera, lens, args.gsd)

    max_speed = get_max_speed(args.shutter_speed, gsd)

    print_kv('Shutter speed', f'{args.shutter_speed}s')
    print_kv('Aperture size', f'f/{args.aperture}')

    print('-' * 42)
    print_kv('GSD', f'{gsd:.2f}cm')
    print_kv('Flight height', f'{flight_height:.1f}m')
    print_kv('Max speed', f'{max_speed:.0f}m/s ~ {max_speed * 3.6:.0f}km/h')

    diffraction = get_diffraction(args.aperture)
    print_kv('Diffraction', f'{diffraction:.5}mm')
