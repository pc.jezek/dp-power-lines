from __future__ import annotations

import json
from argparse import ArgumentParser

import cv2

import powerline3d.utils.calibrate as calib
from powerline3d.utils.io import load_image_paths

arg_parser = ArgumentParser(description="camera calibration")
arg_parser.add_argument('folder', type=str, help="folder with calibration images")
arg_parser.add_argument('--interactive', action='store_true', help="show every image")
arg_parser.add_argument('--resize', type=float, help="rescale images n times")
args = arg_parser.parse_args()

"""
Calibrate camera using Zhang's method
Input: calibration images of checkerbord
Output: camera intrinsics (K, dist)
"""

if __name__ == '__main__':
    images_paths = load_image_paths(args.folder)
    images = [cv2.imread(img) for img in images_paths]

    ret, mtx, dist, rvecs, tvecs = calib.calibrate_camera((6, 9),
                                                          images,
                                                          interactive=args.interactive,
                                                          resize=args.resize, log_to_console=True)
    data = calib.to_opensfm_calibration(images[0].shape[:2], mtx, dist)
    with open('camera_calib.json', 'w') as file:
        json.dump({"all": data}, file, indent=4)
    print(mtx, dist)
