from __future__ import annotations

import argparse
import datetime
import json
import numbers
import os
import pprint

import cv2
import progressbar
import yaml
from pyodm import Node, Task

from powerline3d.utils.io import load_image_paths

"""
A small client for executing OpenDroneMap reconstruction
Input: georeferenced UAV images
Output: various photogrammetric artifacts depending on configuration:
    - undistorted images
    - opensfm reconstruction (intrinsics, extrinsics for each shot)
    - georeferenced point cloud
"""

console_out_len = 0


def print_console_output(output):
    global console_out_len
    if not output:
        return
    output_trimmed = output[console_out_len:]
    console_out_len = len(output)
    if len(output_trimmed):
        _, console_cols = os.popen('stty size', 'r').read().split()
        print('\r' + " " * int(console_cols), end='\r')
        print('\n'.join(output_trimmed))


def load_options(options_file, image_resolution):
    if not options_file:
        return None
    with open(args.options) as opts:
        options = json.load(opts)
        if 'depthmap-resolution' in options and options['depthmap-resolution'] == 'auto':
            options['depthmap-resolution'] = max(image_resolution)
        if 'cameras' in options:
            with open(options['cameras']) as f:
                options['cameras'] = json.dumps(json.load(f))
        outputs = options['outputs']
        del options['outputs']

        pprint.pprint(options)
        return options, outputs


def get_progressbar(name, task: Task = None, debug=False, simple=False):
    timer_widget = progressbar.Timer() if simple else progressbar.Variable('Elapsed')
    bar = progressbar.ProgressBar(
        0, 100,
        widgets=[
            f'{name:<20}',
            ' [', timer_widget, '] ',
            progressbar.Bar(),
            ' (', progressbar.ETA(), ') ',
        ]
    )

    def callback(task_info):
        if isinstance(task_info, numbers.Number):
            bar.update(task_info)
            return

        if task and debug:
            task_info = task.info(True)

        print_console_output(task_info.output)
        if task_info.last_error:
            bar.finish()
            print('Error: ', task_info.last_error)
        else:
            t = datetime.timedelta(seconds=int(task_info.processing_time // 1000))
            bar.update(task_info.progress, Elapsed=str(t))

    return callback


def get_current_task(node):
    tasks = node.get('task/list')
    if not len(tasks):
        print('No running tasks')
        exit(1)
    return node.get_task(tasks[0]['uuid'])


arg_parser = argparse.ArgumentParser()

arg_parser.add_argument('--debug', action='store_true', help="show processing output")
arg_parser.add_argument('--options', type=str, help="use options file (JSON)")
arg_parser.add_argument('--nodownload', action='store_true', help="do not download data")

arg_parser.add_argument('--config',
                        type=str,
                        help="power line reconstruction configuration",
                        default="config/power_lines_cfg.yaml")

group = arg_parser.add_mutually_exclusive_group(required=False)
group.add_argument('--rerun-from', type=str, help="rerun existing task from stage")
group.add_argument('--reconnect', action='store_true', help="reconnect to existing task")
group.add_argument('--reconnect-id', type=str, help="reconnect to existing task with given id")

args = arg_parser.parse_args()
if __name__ == '__main__':

    with open(args.config) as f:
        cfg = yaml.safe_load(f)

    node = Node(cfg['odm']['url'], cfg['odm']['port'])
    images_paths = load_image_paths(cfg['input_image_folder'])

    if not os.path.exists(cfg['odm']['output']):
        os.makedirs(cfg['odm']['output'])

    options, outputs = load_options(args.options, cv2.imread(images_paths[0]).shape)
    if args.rerun_from:
        options.update({'rerun-from': args.rerun_from})
        task = get_current_task(node)
        task.restart(options=options)
    elif args.reconnect:
        task = get_current_task(node)
    elif args.reconnect_id:
        task = node.get_task(args.reconnect_id)
    else:
        task = node.create_task(files=images_paths,
                                options=options,
                                outputs=outputs,
                                progress_callback=get_progressbar('Uploading images', simple=True))
    task.wait_for_completion(status_callback=get_progressbar('Processing', task, args.debug))

    if not args.nodownload:
        task.download_assets(cfg['odm']['output'],
                             progress_callback=get_progressbar('Downloading data', simple=True))
