from __future__ import annotations

import argparse
import os
import pickle
import sys

import cv2
import progressbar
import yaml
from PIL import Image

from powerline3d.reconstruction.utils import basename
from powerline3d.segmentation.prewitt import segment_power_lines_prewitt
from powerline3d.utils.io import load_image_paths

"""
Run Power line segmentation pipeline, supports two methods:
    - neural network segmentation
    - prewitt segmentation
Input: undistorted images
Output: binary segmentation mask for each image
"""

arg_parser = argparse.ArgumentParser()

arg_parser.add_argument('--config',
                        type=str,
                        help="power line reconstruction configuration",
                        default="config/power_lines_cfg.yaml")
arg_parser.add_argument('--device', type=str, help="GPU device", default="cuda:0")
args = arg_parser.parse_args()


def save_result(result, cfg, image_path):
    img = Image.fromarray(result)
    img.save(os.path.join(cfg['segmentation']['output'], basename(image_path) + '.png'))


def segment_prewitt(cfg, image_paths):
    for image_path in progressbar.progressbar(image_paths):
        result = segment_power_lines_prewitt(cv2.imread(image_path))
        save_result(result, cfg, image_path)


def segment_neuralnet(cfg, image_paths):
    from powerline3d.segmentation.neuralnet import segment_power_lines_neuralnet
    from mmseg.apis import init_segmentor
    import mmcv

    with open(cfg['segmentation']['nn_inference']['model_config'], 'rb') as cfg_f:
        model_config = pickle.load(cfg_f)

    model = init_segmentor(model_config, cfg['segmentation']['nn_inference']['model_weights'], device=args.device)

    for image_path in progressbar.progressbar(image_paths):
        img = mmcv.imread(image_path)
        result = segment_power_lines_neuralnet(model, img)
        save_result(result, cfg, image_path)


if __name__ == '__main__':
    with open(args.config) as f:
        cfg = yaml.safe_load(f)

    image_paths = load_image_paths(cfg['undistorted_image_folder'])

    if os.path.exists(cfg['segmentation']['output']):
        print("Output directory already exists:", cfg['segmentation']['output'])
        sys.exit(1)
    os.makedirs(cfg['segmentation']['output'])

    print(f'Segmentation type: {cfg["segmentation"]["type"]}')
    if cfg['segmentation']['type'] == 'prewitt':
        segment_prewitt(cfg, image_paths)
    else:
        segment_neuralnet(cfg, image_paths)
