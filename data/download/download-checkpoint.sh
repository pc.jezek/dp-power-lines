#!/bin/bash

OUT="$1/segmentation/checkpoints"
URL='https://download.openmmlab.com/mmsegmentation/v0.5/deeplabv3plus/deeplabv3plus_r101-d8_512x1024_80k_cityscapes/deeplabv3plus_r101-d8_512x1024_80k_cityscapes_20200606_114143-068fcfe9.pth'

mkdir -p "$OUT"
wget "$URL" -P "$OUT"

