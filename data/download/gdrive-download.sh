#!/bin/bash

ID="$1"
OUT="$2"
COOKIES="$OUT/_cookies.txt"
CONFIRM="$OUT/_confirm"

printusage(){ 
  echo "Usage: $0 <gdrive-id> <output-directory>" 
}

cleanup(){
  rm -f "$OUT/cookies.txt" "$OUT/confirm"
}

error(){
  echo $1
  rm -f "$OUT/data.zip"
  cleanup
}

[ -z $1 ] && printusage && exit 1
[ -z $2 ] && printusage && exit 1

rm -rf $OUT
mkdir -p "$OUT"

wget -q --save-cookies "$OUT/cookies.txt" --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=$ID" -O- \
| sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p' > "$OUT/confirm"

wget -q --show-progress --load-cookies "$OUT/cookies.txt" --no-check-certificate \
  "https://docs.google.com/uc?export=download&confirm="$(cat "$OUT/confirm")"&id=$ID" -O "$OUT/data.zip"

head -c 1000 "$OUT/data.zip" | grep -qi 'quota exceeded' && error "Error: Quota Exceeded" && exit 2

cleanup
