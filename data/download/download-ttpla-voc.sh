#!/bin/bash

SCRIPT_DIR="$(dirname "$0")"

OUT="$1/ttpla-voc"
ID="1tNgwpgBhzVJUhoDlhHQzNuFRjx_Ep6Wu"

printusage(){ 
  echo "Usage: $0 <output-directory>" 
}

error(){
  cleanup
  exit 2
}

cleanup(){
  rm -rf "$OUT/data.zip"
}

[ -z $1 ] && printusage && exit 1

"$SCRIPT_DIR/gdrive-download.sh" $ID $OUT || error

unzip "$OUT/data.zip" -d "$OUT"
cleanup
